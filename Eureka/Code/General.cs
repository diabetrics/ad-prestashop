﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Eureka.Code
{
    public class General
    {
       static string _BaseUrl = ConfigurationManager.AppSettings["Server"];
       static string _APIAccount = ConfigurationManager.AppSettings["User"];
        static string _APIPassword = "";

        public static string BaseUrl { get { return _BaseUrl; } }
        public static string APIAccount { get { return _APIAccount; } }
        public static string APIPassword { get { return _APIPassword; } }
    }
}