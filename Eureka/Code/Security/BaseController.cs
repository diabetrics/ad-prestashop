﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eureka.Code.Security
{
    public class BaseController : Controller
    {
       protected virtual new PrixmaPrincipal User
        {
            get { return HttpContext.User as PrixmaPrincipal; }
        }
        protected virtual bool IsAuthenticated
        {
            get { return HttpContext.User.Identity.IsAuthenticated; }
        }
    }
}