﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Bukimedia.PrestaSharp.Entities;
using Bukimedia.PrestaSharp.Factories;
using System.Configuration;
using Eureka.Models;

namespace Eureka.Code.Security
{
    public class PrixmaPrincipal : IPrincipal
    {
        public eur_account Account;
        public bool IsShopAdmin= false;
        public bool IsCMSAdmin = false;
        public bool IsShopUser = false;
        public bool IsCMSUser = false;
        string BaseUrl = ConfigurationManager.AppSettings["Server"];
        string APIAccount = ConfigurationManager.AppSettings["User"];
        string APIPassword = "";
        public IIdentity Identity
        {
            get; private set;
        }

        public bool IsInRole(string role)
        {
            switch (role)
            {
                case "CMSADMIN": return IsCMSAdmin;
                case "SHOPADMIN": return IsShopAdmin;
                default: return false;
            }
        }
        public PrixmaPrincipal(eur_account Acc)
        {
            Account = Acc;
            IsShopUser = Utils.IsShopUser(Acc.ACP_Mail);
            if (!IsShopUser)
            {
                Utils.CreateShopUser(Acc);
                IsShopUser = true;
            }
            IsCMSUser = Utils.IsCMSUser(Acc.ACP_User);
            IsCMSAdmin = Utils.IsCMSAdmin(Acc.ACP_User);
            if (!IsCMSUser && !IsCMSAdmin)
            {
                Utils.CreateCMSUser(Acc);
                IsCMSUser = true;
            }

            /*using (var factory = new CustomerFactory(BaseUrl, APIAccount, APIPassword))
            {
                Dictionary<string, string> dtn = new Dictionary<string, string>();
                dtn.Add("email", Acc.mail);
                Customer = factory.GetByFilter(dtn, null, null).FirstOrDefault();
            }
            using (var factory = new EmployeeFactory(BaseUrl, APIAccount, APIPassword))
            {
                Dictionary<string, string> dtn = new Dictionary<string, string>();
                dtn.Add("email", Customer.email);
                Employee = factory.GetByFilter(dtn, null, null).FirstOrDefault();
                IsShopAmdin = Employee != null;
            }*/
            this.Identity = new GenericIdentity(Acc.ACP_User);

        }
    }
}