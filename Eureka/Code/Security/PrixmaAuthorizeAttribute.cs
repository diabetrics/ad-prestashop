﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eureka.Code.Security
{
    public class PrixmaAuthorizeAttribute: AuthorizeAttribute
    {
        protected virtual PrixmaPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as PrixmaPrincipal; }
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                PrixmaPrincipal mp = new PrixmaPrincipal(CurrentUser.Account);
                if (!mp.IsInRole(Roles))
                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "AccesoDenegado", action = "Index" }));
            }
            else
            {
                filterContext.Result =
                    new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Home", action = "Index" }));
            }
        }
    }
}