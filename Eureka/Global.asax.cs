﻿using Eureka.Code.Security;
using Eureka.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Eureka
{
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }
    protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
    {
      HttpCookie auth = Request.Cookies[FormsAuthentication.FormsCookieName];
      if (auth != null)
      {
        FormsAuthenticationTicket authti = FormsAuthentication.Decrypt(auth.Value);
        eur_account model = JsonConvert.DeserializeObject<eur_account>(authti.UserData);
        PrixmaPrincipal newuser = new PrixmaPrincipal(model);
        newuser.Account = model;
        HttpContext.Current.User = newuser;
      }
    }
  }
}
