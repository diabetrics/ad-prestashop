﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Eureka.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class InngeniaEntities : DbContext
    {
        public InngeniaEntities()
            : base("name=InngeniaEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<eur_account> eur_account { get; set; }
        public virtual DbSet<eur_areaprofile> eur_areaprofile { get; set; }
        public virtual DbSet<eur_campaincriteria> eur_campaincriteria { get; set; }
        public virtual DbSet<eur_campaingeneral> eur_campaingeneral { get; set; }
        public virtual DbSet<eur_campainprofile> eur_campainprofile { get; set; }
        public virtual DbSet<eur_campainreq> eur_campainreq { get; set; }
        public virtual DbSet<eur_enterpriseprofile> eur_enterpriseprofile { get; set; }
        public virtual DbSet<eur_evalcriteria> eur_evalcriteria { get; set; }
        public virtual DbSet<eur_evalgeneral> eur_evalgeneral { get; set; }
        public virtual DbSet<eur_evalreq> eur_evalreq { get; set; }
        public virtual DbSet<eur_facetprofile> eur_facetprofile { get; set; }
        public virtual DbSet<eur_ideadocument> eur_ideadocument { get; set; }
        public virtual DbSet<eur_ideafacet> eur_ideafacet { get; set; }
        public virtual DbSet<eur_ideapartner> eur_ideapartner { get; set; }
        public virtual DbSet<eur_ideapoints> eur_ideapoints { get; set; }
        public virtual DbSet<eur_ideaprofile> eur_ideaprofile { get; set; }
        public virtual DbSet<eur_ideasponsor> eur_ideasponsor { get; set; }
        public virtual DbSet<eur_ideastateprofile> eur_ideastateprofile { get; set; }
        public virtual DbSet<eur_projectdocument> eur_projectdocument { get; set; }
        public virtual DbSet<eur_projectpartner> eur_projectpartner { get; set; }
        public virtual DbSet<eur_projectprofile> eur_projectprofile { get; set; }
    }
}
