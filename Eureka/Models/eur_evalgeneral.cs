//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Eureka.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class eur_evalgeneral
    {
        public int CEG_AutoID { get; set; }
        public int CEG_CampainID { get; set; }
        public int CEG_IDPID { get; set; }
        public string CEG_Value { get; set; }
        public System.DateTime CEG_AddDate { get; set; }
        public string CEG_AddUser { get; set; }
        public System.DateTime CEG_EditDate { get; set; }
        public string CEG_EditUser { get; set; }
    }
}
