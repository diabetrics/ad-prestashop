﻿using ActiveDirectoryWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActiveDirectoryWebAPI.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Login(LoginViewModel model)
        {
            var domain = ConfigurationManager.AppSettings["dominio"];
            if (ModelState.IsValid)
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
                {
                    bool isValid = pc.ValidateCredentials(model.UserName, model.Password);
                    if (isValid)
                    {
                        var data = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, domain + "\\" + model.UserName);
                        Account cliente = new Account
                        {
                            mail = data.EmailAddress,
                            cedula = data.EmployeeId,
                            nombre = data.GivenName,
                            apellido = data.Surname,
                            user = data.Name
                        };

                    }
                    RedirectToAction("Index", "Dash");
                }
            }
            return View(model);
        }
    }
}