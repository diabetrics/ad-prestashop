﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.DirectoryServices.AccountManagement;
using Bukimedia.PrestaSharp;
using Bukimedia.PrestaSharp.Factories;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
using Fluentx.Mvc;
using ActiveDirectoryWebAPI.Models;

namespace ActiveDirectoryWebAPI.Controllers
{
    public class ADController : Controller
    {
        string BaseUrl = ConfigurationManager.AppSettings["Server"];
        string Account = ConfigurationManager.AppSettings["User"];
        string Password = "";
        // GET: AD
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(string domain, string user, string pass)
        {
            var rshop = ConfigurationManager.AppSettings["rshop"];
            var rerror = ConfigurationManager.AppSettings["rerror"];
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
            {
                // validate the credentials
                bool isValid = pc.ValidateCredentials(user, pass);
                if (isValid)
                {

                    var data = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, domain + "\\" + user);
                    Account cliente = new Account
                    {
                        mail = data.EmailAddress,
                        cedula = data.EmployeeId,
                        nombre = data.GivenName,
                        apellido = data.Surname,
                        user = data.Name
                    };

                    AddressFactory adfac = new AddressFactory(BaseUrl, Account, Password);
                    CustomerFactory factory = new CustomerFactory(BaseUrl, Account, Password);
                    Dictionary<string, string> dtn = new Dictionary<string, string>();
                    dtn.Add("email", cliente.mail);
                    var existe = factory.GetByFilter(dtn, null, null);
                    var res = "Existe";
                    if (existe.Count == 0)
                    {
                        Bukimedia.PrestaSharp.Entities.customer cus = new Bukimedia.PrestaSharp.Entities.customer()
                        {
                            active = 1,
                            date_add = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            email = cliente.mail,
                            firstname = cliente.nombre,
                            id_shop = 1,
                            id_shop_group = 1,
                            id_default_group = 3,
                            id_lang = 3,
                            id_risk = 0,
                            lastname = cliente.apellido,
                            id_gender = 1,
                            ape = null,
                            birthday = null,
                            company = null,
                            date_upd = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            deleted = 0,
                            siret = null,
                            passwd = "",
                            last_passwd_gen = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            newsletter = 1,
                            ip_registration_newsletter = null,
                            newsletter_date_add = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            optin = 1,
                            website = null,
                            outstanding_allow_amount = 0,
                            show_public_prices = 0,
                            max_payment_days = 0,
                            note = null,
                            is_guest = 0,

                        };
                        factory.Add(cus);
                        cus = factory.GetByFilter(dtn, null, null).First();

                        Bukimedia.PrestaSharp.Entities.address addr = new Bukimedia.PrestaSharp.Entities.address()
                        {
                            address1 = "Calle 80 # 78B - 201",
                            address2 = "",
                            alias = "Direccion",
                            city = "Barranquilla",
                            company = "Procaps",
                            date_add = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            date_upd = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            deleted = 0,
                            dni = cliente.cedula,
                            firstname = cliente.nombre,
                            id_country = 69,
                            id_customer = cus.id,
                            id_manufacturer = 0,
                            id_state = 0,
                            id_supplier = 0,
                            id_warehouse = 0,
                            lastname = cliente.apellido,
                            phone = "",
                            phone_mobile = "",
                            other = "",
                            postcode = "",
                            vat_number = ""

                        };
                        adfac.Add(addr);
                        res = "Creado";
                    }
                    Dictionary<string, object> postData = new Dictionary<string, object>();
                    postData.Add("email", cliente.mail);
                    return this.RedirectAndPost(rshop, postData);
                    //return Redirect("http://test.diabetrics.tienda/functions.php");
                    //return Json(new { data = cliente, res = res }, JsonRequestBehavior.AllowGet);
                }
            }
            return Redirect(rerror);
        }
    }
}