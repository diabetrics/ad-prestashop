﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ActiveDirectoryWebAPI.Models
{
    public class LoginViewModel
    {
        [Required, DisplayName("Nombre de Usuario")]
        public string UserName { get; set; }
        [Required, DisplayName("Contraseña"), DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class Account
    {
        public string mail { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string user { get; set; }

    }
}