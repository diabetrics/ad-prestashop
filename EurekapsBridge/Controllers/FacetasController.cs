﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
    [PrixmaAuthorize]
  public class FacetasController : BaseController
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: Facetas
    public ActionResult Index()
    {
      return View(db.eur_facetprofile.ToList());
    }

    // GET: Facetas/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_facetprofile eur_facetprofile = db.eur_facetprofile.Find(id);
      if (eur_facetprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_facetprofile);
    }

    // GET: Facetas/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Facetas/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "FP_AutoID,FP_Name,FP_Description,FP_State,FP_AddDate,FP_AddUser,FP_EditDate,FP_EditUser")] eur_facetprofile eur_facetprofile)
    {
      if (ModelState.IsValid)
      {
        eur_facetprofile.FP_AddDate = DateTime.Now;
        eur_facetprofile.FP_AddUser = User.Account.ACP_User;
        eur_facetprofile.FP_EditDate = DateTime.Now;
        eur_facetprofile.FP_EditUser = User.Account.ACP_User;
        db.eur_facetprofile.Add(eur_facetprofile);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_facetprofile);
    }

    // GET: Facetas/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_facetprofile eur_facetprofile = db.eur_facetprofile.Find(id);
      if (eur_facetprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_facetprofile);
    }

    // POST: Facetas/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "FP_AutoID,FP_Name,FP_Description,FP_State,FP_AddDate,FP_AddUser,FP_EditDate,FP_EditUser")] eur_facetprofile eur_facetprofile)
    {
      if (ModelState.IsValid)
      {
        eur_facetprofile.FP_EditDate = DateTime.Now;
        eur_facetprofile.FP_EditUser = User.Account.ACP_User;
        db.Entry(eur_facetprofile).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_facetprofile);
    }

    // GET: Facetas/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_facetprofile eur_facetprofile = db.eur_facetprofile.Find(id);
      if (eur_facetprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_facetprofile);
    }

    // POST: Facetas/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_facetprofile eur_facetprofile = db.eur_facetprofile.Find(id);
      db.eur_facetprofile.Remove(eur_facetprofile);
      db.SaveChanges();
      return RedirectToAction("Index");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
