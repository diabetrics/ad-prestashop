﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
    [PrixmaAuthorize]
  public class RequerimientosCampanaController : Controller
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: RequerimientosCampana
    public ActionResult Index()
    {
      return View(db.eur_campainreq.ToList());
    }

    // GET: RequerimientosCampana/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campainreq eur_campainreq = db.eur_campainreq.Find(id);
      if (eur_campainreq == null)
      {
        return HttpNotFound();
      }
      return View(eur_campainreq);
    }

    // GET: RequerimientosCampana/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: RequerimientosCampana/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "CMR_AutoID,CMR_CampainID,CMR_Name,CMR_Description,CMR_Type,CMR_State,CMR_AddDate,CMR_AddUser,CMR_EditDate,CMR_EditUser")] eur_campainreq eur_campainreq)
    {
      if (ModelState.IsValid)
      {
        db.eur_campainreq.Add(eur_campainreq);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_campainreq);
    }
    [HttpPost]
    public JsonResult CreateVM(string Name, string Description, string Type, string campain)
    {
      var status = true;
      var ent = new eur_campainreq()
      {
        CMR_AddDate = DateTime.Now,
        CMR_AddUser = User.Identity.Name,
        CMR_CampainID = campain,
        CMR_Description = Description,
        CMR_EditDate = DateTime.Now,
        CMR_EditUser = User.Identity.Name,
        CMR_Name = Name,
        CMR_State = "A",
        CMR_Type = Type
      };
      db.eur_campainreq.Add(ent);
      db.SaveChanges();
      db.Entry(ent).GetDatabaseValues();
      return new JsonResult { Data = new { status = status, id = ent.CMR_AutoID } };
    }
    // GET: RequerimientosCampana/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campainreq eur_campainreq = db.eur_campainreq.Find(id);
      if (eur_campainreq == null)
      {
        return HttpNotFound();
      }
      return View(eur_campainreq);
    }

    // POST: RequerimientosCampana/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "CMR_AutoID,CMR_CampainID,CMR_Name,CMR_Description,CMR_Type,CMR_State,CMR_AddDate,CMR_AddUser,CMR_EditDate,CMR_EditUser")] eur_campainreq eur_campainreq)
    {
      if (ModelState.IsValid)
      {
        db.Entry(eur_campainreq).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_campainreq);
    }

    // GET: RequerimientosCampana/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campainreq eur_campainreq = db.eur_campainreq.Find(id);
      if (eur_campainreq == null)
      {
        return HttpNotFound();
      }
      return View(eur_campainreq);
    }

    // POST: RequerimientosCampana/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_campainreq eur_campainreq = db.eur_campainreq.Find(id);
      db.eur_campainreq.Remove(eur_campainreq);
      db.SaveChanges();
      return RedirectToAction("Index");
    }
    [HttpPost]
    public ActionResult DeleteVM(int id)
    {
      eur_campainreq eur_campainreq = db.eur_campainreq.Find(id);
      db.eur_campainreq.Remove(eur_campainreq);
      db.SaveChanges();
      return new JsonResult { Data = new { status = true } };
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
