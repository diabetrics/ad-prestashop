﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;

namespace EurekapsBridge.Controllers
{
    public class IdeaIndicadorController : Controller
    {
        private InngeniaEntities db = new InngeniaEntities();

        // GET: IdeaIndicador
        public ActionResult Index()
        {
            return View(db.eur_ideaindicators.ToList());
        }

        // GET: IdeaIndicador/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideaindicators eur_ideaindicators = db.eur_ideaindicators.Find(id);
            if (eur_ideaindicators == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideaindicators);
        }

        // GET: IdeaIndicador/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IdeaIndicador/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "II_AutoID,II_Description,II_State,II_AddDate,II_AddUser,II_EditDate,II_EditUser")] eur_ideaindicators eur_ideaindicators)
        {
            if (ModelState.IsValid)
            {
                db.eur_ideaindicators.Add(eur_ideaindicators);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eur_ideaindicators);
        }

        // GET: IdeaIndicador/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideaindicators eur_ideaindicators = db.eur_ideaindicators.Find(id);
            if (eur_ideaindicators == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideaindicators);
        }

        // POST: IdeaIndicador/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "II_AutoID,II_Description,II_State,II_AddDate,II_AddUser,II_EditDate,II_EditUser")] eur_ideaindicators eur_ideaindicators)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eur_ideaindicators).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eur_ideaindicators);
        }

        // GET: IdeaIndicador/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideaindicators eur_ideaindicators = db.eur_ideaindicators.Find(id);
            if (eur_ideaindicators == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideaindicators);
        }

        // POST: IdeaIndicador/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eur_ideaindicators eur_ideaindicators = db.eur_ideaindicators.Find(id);
            db.eur_ideaindicators.Remove(eur_ideaindicators);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    [HttpPost]
    public ActionResult CreateVM(string Desc, int id, string Name)
    {
      eur_ideaindicators indicador = new eur_ideaindicators
      {
        II_AddDate = DateTime.Now,
        II_AddUser = User.Identity.Name,
        II_Description = Desc,
        II_EditDate = DateTime.Now,
        II_EditUser = User.Identity.Name,
        II_State = "N",
        II_Name = Name,
        II_IDPID = id
        
      };
      db.eur_ideaindicators.Add(indicador);
      db.SaveChanges();
      db.Entry(indicador).GetDatabaseValues();
      return new JsonResult { Data = new { status = true, id = indicador.II_AutoID } };
      

      
    }
    [HttpPost]
    public ActionResult DeleteRow(int id)
    {
      eur_ideaindicators eur_ideaindicators = db.eur_ideaindicators.Find(id);
      db.eur_ideaindicators.Remove(eur_ideaindicators);
      db.SaveChanges();
      return new JsonResult { Data=new { status = true } };
    }
    protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
