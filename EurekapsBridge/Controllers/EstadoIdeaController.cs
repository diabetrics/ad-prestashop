﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
    [PrixmaAuthorize]
  public class EstadoIdeaController : BaseController
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: EstadoIdea
    public ActionResult Index()
    {
      return View(db.eur_ideastateprofile.ToList());
    }

    // GET: EstadoIdea/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideastateprofile eur_ideastateprofile = db.eur_ideastateprofile.Find(id);
      if (eur_ideastateprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideastateprofile);
    }

    // GET: EstadoIdea/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: EstadoIdea/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "ISP_AutoID,ISP_Name,ISP_Description,ISP_State,ISP_AddDate,ISP_AddUser,ISP_EditDate,ISP_EditUser")] eur_ideastateprofile eur_ideastateprofile)
    {
      if (ModelState.IsValid)
      {
        eur_ideastateprofile.ISP_AddUser = User.Account.ACP_User;
        eur_ideastateprofile.ISP_AddDate = DateTime.Now;
        eur_ideastateprofile.ISP_EditUser = User.Account.ACP_User;
        eur_ideastateprofile.ISP_EditDate = DateTime.Now;
        db.eur_ideastateprofile.Add(eur_ideastateprofile);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_ideastateprofile);
    }

    // GET: EstadoIdea/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideastateprofile eur_ideastateprofile = db.eur_ideastateprofile.Find(id);
      if (eur_ideastateprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideastateprofile);
    }

    // POST: EstadoIdea/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "ISP_AutoID,ISP_Name,ISP_Description,ISP_State,ISP_AddDate,ISP_AddUser,ISP_EditDate,ISP_EditUser")] eur_ideastateprofile eur_ideastateprofile)
    {
      if (ModelState.IsValid)
      {
        eur_ideastateprofile.ISP_EditUser = User.Account.ACP_User;
        eur_ideastateprofile.ISP_EditDate = DateTime.Now;

        db.Entry(eur_ideastateprofile).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_ideastateprofile);
    }

    // GET: EstadoIdea/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideastateprofile eur_ideastateprofile = db.eur_ideastateprofile.Find(id);
      if (eur_ideastateprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideastateprofile);
    }

    // POST: EstadoIdea/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_ideastateprofile eur_ideastateprofile = db.eur_ideastateprofile.Find(id);
      db.eur_ideastateprofile.Remove(eur_ideastateprofile);
      db.SaveChanges();
      return RedirectToAction("Index");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
