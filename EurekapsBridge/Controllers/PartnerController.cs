﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;

namespace EurekapsBridge.Controllers
{
    public class PartnerController : Controller
    {
        private InngeniaEntities db = new InngeniaEntities();

        // GET: Partner
        public ActionResult Index()
        {
            return View(db.eur_ideapartner.ToList());
        }

        // GET: Partner/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideapartner eur_ideapartner = db.eur_ideapartner.Find(id);
            if (eur_ideapartner == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideapartner);
        }

        // GET: Partner/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Partner/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IP_AutoID,IP_IDPID,IP_User,IP_State,IP_AddDate,IP_AddUser,IP_EditDate,IP_EditUser")] eur_ideapartner eur_ideapartner)
        {
            if (ModelState.IsValid)
            {
                db.eur_ideapartner.Add(eur_ideapartner);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eur_ideapartner);
        }

        // GET: Partner/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideapartner eur_ideapartner = db.eur_ideapartner.Find(id);
            if (eur_ideapartner == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideapartner);
        }

        // POST: Partner/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IP_AutoID,IP_IDPID,IP_User,IP_State,IP_AddDate,IP_AddUser,IP_EditDate,IP_EditUser")] eur_ideapartner eur_ideapartner)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eur_ideapartner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eur_ideapartner);
        }

        // GET: Partner/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideapartner eur_ideapartner = db.eur_ideapartner.Find(id);
            if (eur_ideapartner == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideapartner);
        }

        // POST: Partner/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eur_ideapartner eur_ideapartner = db.eur_ideapartner.Find(id);
            db.eur_ideapartner.Remove(eur_ideapartner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    public JsonResult CreateVM(string Name, int id)
    {
      eur_ideapartner ent = db.eur_ideapartner.Where(a => a.IP_User == Name&& a.IP_IDPID == id).FirstOrDefault();
      if (ent != null)
      {
        return new JsonResult { Data = new { status = false } };
      }
      else
      {
        ent = new eur_ideapartner
        {
          IP_AddDate = DateTime.Now,
          IP_AddUser = User.Identity.Name,
          IP_EditDate = DateTime.Now,
          IP_EditUser = User.Identity.Name,
          IP_IDPID = id,
          IP_State = "A",
          IP_User = Name
        };
        db.eur_ideapartner.Add(ent);
        db.SaveChanges();
        db.Entry(ent).GetDatabaseValues();
        return new JsonResult { Data = new { status = true, id = ent.IP_AutoID } };
      }
    }
    // POST: Partner/Delete/5
    [HttpPost]
    public ActionResult DeleteRow(int id)
    {
      eur_ideapartner eur_ideapartner = db.eur_ideapartner.Find(id);
      db.eur_ideapartner.Remove(eur_ideapartner);
      db.SaveChanges();
      return new JsonResult { Data = new { status = true } };
    }
    

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
