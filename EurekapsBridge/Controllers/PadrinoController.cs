﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;

namespace EurekapsBridge.Controllers
{
    public class PadrinoController : Controller
    {
        private InngeniaEntities db = new InngeniaEntities();

        // GET: Padrino
        public ActionResult Index()
        {
            return View(db.eur_ideasponsor.ToList());
        }

        // GET: Padrino/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideasponsor eur_ideasponsor = db.eur_ideasponsor.Find(id);
            if (eur_ideasponsor == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideasponsor);
        }

        // GET: Padrino/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Padrino/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IS_AutoID,IS_IDPID,IS_User,IS_State,IS_AddDate,IS_AddUser,IS_EditDate,IS_EditUser")] eur_ideasponsor eur_ideasponsor)
        {
            if (ModelState.IsValid)
            {
                db.eur_ideasponsor.Add(eur_ideasponsor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eur_ideasponsor);
        }

        // GET: Padrino/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideasponsor eur_ideasponsor = db.eur_ideasponsor.Find(id);
            if (eur_ideasponsor == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideasponsor);
        }

        // POST: Padrino/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IS_AutoID,IS_IDPID,IS_User,IS_State,IS_AddDate,IS_AddUser,IS_EditDate,IS_EditUser")] eur_ideasponsor eur_ideasponsor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eur_ideasponsor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eur_ideasponsor);
        }

        // GET: Padrino/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_ideasponsor eur_ideasponsor = db.eur_ideasponsor.Find(id);
            if (eur_ideasponsor == null)
            {
                return HttpNotFound();
            }
            return View(eur_ideasponsor);
        }

        // POST: Padrino/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eur_ideasponsor eur_ideasponsor = db.eur_ideasponsor.Find(id);
            db.eur_ideasponsor.Remove(eur_ideasponsor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    public JsonResult CreateVM(string Name, int id)
    {
      eur_ideasponsor ent = db.eur_ideasponsor.Where(a => a.IS_User == Name && a.IS_IDPID == id).FirstOrDefault();
      if (ent != null)
      {
        return new JsonResult { Data = new { status = false } };
      }
      else
      {
        ent = new eur_ideasponsor
        {
          IS_AddDate = DateTime.Now,
          IS_AddUser = User.Identity.Name,
          IS_EditDate = DateTime.Now,
          IS_EditUser = User.Identity.Name,
          IS_IDPID = id,
          IS_State = "A",
          IS_User = Name
        };
        db.eur_ideasponsor.Add(ent);
        db.SaveChanges();
        db.Entry(ent).GetDatabaseValues();
        return new JsonResult { Data = new { status = true, id = ent.IS_AutoID } };
      }
    }
    // POST: Partner/Delete/5
    [HttpPost]
    public ActionResult DeleteRow(int id)
    {
      eur_ideasponsor eur = db.eur_ideasponsor.Find(id);
      db.eur_ideasponsor.Remove(eur);
      db.SaveChanges();
      return new JsonResult { Data = new { status = true } };
    }
    protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
