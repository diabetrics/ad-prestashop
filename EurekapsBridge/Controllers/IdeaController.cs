﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using System.IO;
using System.Data.Entity.Validation;
using EurekapsBridge.Code.Helpers;
using EurekapsBridge.Code.Security;
using System.Configuration;

namespace EurekapsBridge.Controllers
{
  [Authorize]
  public class IdeaController : BaseController
  {
    private InngeniaEntities db = new InngeniaEntities();

    public object HelperUtils { get; private set; }
    public string ConfigurationMannage { get; private set; }

    // GET: Idea
    public ActionResult Index()
    {
      var ven = (from a in db.eur_ideaprofile
                 join c in db.eur_campainprofile on a.IDP_CampainID equals c.CAM_CampainID
                 where a.IDP_Status == "ID" && a.IDP_AddUser == User.Identity.Name
                 select new { idea = a, campaña = c }).ToList();
      ViewBag.Vencidas = from v in ven
                         where v.campaña.CAM_FechaIdea.AddDays(1) < DateTime.Now
                         select v.idea;
      
      var ldato = from v in ven
                  where v.campaña.CAM_FechaIdea.AddDays(1) >= DateTime.Now
                  select v.idea;
      return View(ldato);
    }
    public ActionResult IndexIN()
    {
      var compañero = from p in db.eur_ideapartner
                      join i in db.eur_ideaprofile on p.IP_IDPID equals i.IDP_AutoID
                      where p.IP_User == User.Identity.Name
                      select i;
      ViewBag.Compañero = compañero.ToList();

      var padrino = from p in db.eur_ideasponsor
                    join i in db.eur_ideaprofile on p.IS_IDPID equals i.IDP_AutoID
                    where p.IS_User == User.Identity.Name
                    select i;
      ViewBag.Padrino = padrino.ToList();

      var ven = (from a in db.eur_ideaprofile
                 join c in db.eur_campainprofile on a.IDP_CampainID equals c.CAM_CampainID
                 where a.IDP_Status == "IN" && a.IDP_AddUser == User.Identity.Name
                 select new { idea = a, campaña = c }).ToList();

      var EF = (from a in db.eur_ideaprofile
                join c in db.eur_campainprofile on a.IDP_CampainID equals c.CAM_CampainID
                where a.IDP_Status == "EF" && a.IDP_AddUser == User.Identity.Name
                select new { idea = a, campaña = c }).ToList();

      ViewBag.Feedbak = from v in EF
                        where v.campaña.CAM_FechaIniciativa.AddDays(15) >= DateTime.Now
                        select v.idea; ;

      ViewBag.Vencidas = from v in ven
                         where v.campaña.CAM_FechaIniciativa.AddDays(1) < DateTime.Now
                         select v.idea;

      var ldato = from v in ven
                  where v.campaña.CAM_FechaIniciativa.AddDays(1) >= DateTime.Now
                  select v.idea;

      return View(ldato);
    }

    // GET: Idea/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideaprofile eur_ideaprofile = db.eur_ideaprofile.Find(id);

      if (eur_ideaprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideaprofile);
    }

    // GET: Idea/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Idea/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "IDP_AutoID,IDP_Name,IDP_Description,IDP_APID,IDP_Comment,IDP_EPID,IDP_PreIdea,IDP_PostIdea,IDP_Scope,IDP_Schedule,IDP_DescInvest,IDP_Investment,IDP_InKind,IDP_Validation,IDP_Benefits,IDP_BenefitsIndicators,IDP_ISPID,IDP_Feedback,IDP_AddDate,IDP_AddUser,IDP_ModifyDate,IDP_ModifyUser")] eur_ideaprofile eur_ideaprofile)
    {
      if (ModelState.IsValid)
      {
        db.eur_ideaprofile.Add(eur_ideaprofile);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_ideaprofile);
    }
    // GET: Idea/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideaprofile eur_ideaprofile = db.eur_ideaprofile.Find(id);
      if (!Utilshelper.IsMyIdea(eur_ideaprofile))
      {
        throw new Exception("Alguien intento modificar la idea de: " + eur_ideaprofile.IDP_AddUser);
      }
      if (eur_ideaprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideaprofile);
    }

    // POST: Idea/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "IDP_AutoID,IDP_Name,IDP_Description,IDP_APID,IDP_Comment,IDP_EPID,IDP_PreIdea,IDP_PostIdea,IDP_Scope,IDP_Schedule,IDP_DescInvest,IDP_Investment,IDP_InKind,IDP_Validation,IDP_Benefits,IDP_BenefitsIndicators,IDP_ISPID,IDP_Feedback,IDP_AddDate,IDP_AddUser,IDP_ModifyDate,IDP_ModifyUser")] eur_ideaprofile eur_ideaprofile)
    {
      if (!Utilshelper.IsMyIdea(eur_ideaprofile))
      {
        throw new Exception("Alguien intento modificar la idea de: " + eur_ideaprofile.IDP_AddUser);
      }
      if (ModelState.IsValid)
      {
        db.Entry(eur_ideaprofile).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_ideaprofile);
    }

    // GET: Idea/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideaprofile eur_ideaprofile = db.eur_ideaprofile.Find(id);
      if (eur_ideaprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideaprofile);
    }

    // POST: Idea/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_ideaprofile eur_ideaprofile = db.eur_ideaprofile.Find(id);
      db.eur_ideaprofile.Remove(eur_ideaprofile);
      db.SaveChanges();
      return RedirectToAction("Index");
    }
    public ActionResult ErrorFecha()
    {
      return View();
    }
    public ActionResult CreateIdea(string CampainID)
    {
      eur_campainprofile cam = db.eur_campainprofile.Where(a => a.CAM_CampainID == CampainID).FirstOrDefault();
      if (DateTime.Now > cam.CAM_FechaIdea)
      {
        throw new Exception("La campaña ya habia cerrado");
      }
      IdeaViewModel model = new IdeaViewModel
      {
        IDP_CampainID = CampainID
      };
      return View(model);
    }
    [HttpPost]
    public ActionResult CreateIdea(IdeaViewModel model)
    {
      try {
        if (ModelState.IsValid)
        {
          eur_ideaprofile idea = new eur_ideaprofile
          {
            IDP_AddDate = DateTime.Now,
            IDP_AddUser = User.Identity.Name,
            IDP_APID = model.IDP_APID,
            IDP_CampainID = model.IDP_CampainID,
            IDP_Comment = model.IDP_Comment,
            IDP_Description = model.IDP_Description,
            IDP_EPID = model.IDP_EPID,
            IDP_ModifyDate = DateTime.Now,
            IDP_ModifyUser = User.Identity.Name,
            IDP_Name = model.IDP_Name,
            IDP_Status = "ID"
          };
          db.eur_ideaprofile.Add(idea);
          db.SaveChanges();
          db.Entry(idea).GetDatabaseValues();

          if (model.Upload != null)
          {
            if (model.Upload.ContentLength > 0)
            {
              DirectoryInfo dir = new DirectoryInfo(Server.MapPath("~/Documentos/Idea/" + idea.IDP_AutoID));
              if (!dir.Exists)
              {
                dir.Create();
              }
              string path = Path.Combine(dir.ToString(), Path.GetFileName(model.Upload.FileName));
              eur_ideadocument doc = new eur_ideadocument
              {
                ID_AddDate = DateTime.Now,
                ID_AddUser = User.Identity.Name,
                ID_EditDate = DateTime.Now,
                ID_EditUser = User.Identity.Name,
                ID_IDPID = idea.IDP_AutoID,
                ID_Path = "Idea/" + idea.IDP_AutoID + "/" + model.Upload.FileName,
                ID_State = "A",
                ID_Type = "S"
              };
              db.eur_ideadocument.Add(doc);
              db.SaveChanges();
              model.Upload.SaveAs(path);
            }
          }
          if (model.IDP_CampainID != ConfigurationManager.AppSettings["CampañaPrueba"])
          {
            eur_ideapoints points = db.eur_ideapoints.Where(a => a.IUP_User == User.Identity.Name && a.IUP_CAMID == model.IDP_CampainID).FirstOrDefault();
            if (points == null)
            {
              points = new eur_ideapoints
              {
                IUP_AddDate = DateTime.Now,
                IUP_AddUser = User.Identity.Name,
                IUP_CAMID = model.IDP_CampainID,
                IUP_EditDate = DateTime.Now,
                IUP_EditUser = User.Identity.Name,
                IUP_IDPID = idea.IDP_AutoID,
                IUP_Points = 10,
                IUP_Type = "ID",
                IUP_User = User.Identity.Name
              };
              db.eur_ideapoints.Add(points);
              db.SaveChanges();
              Utilshelper.AsignarShopPoints(10, User.Identity.Name);
            }
          }

          Dictionary<string, string> valores = new Dictionary<string, string>();
          valores.Add("Nombre", User.Account.ACP_FirstName);
          valores.Add("IdeaName", model.IDP_Name);
          valores.Add("CampainName", model.IDP_CampainName);
          valores.Add("FechaCierre", idea.IDP_CierrerIdea.ToString("yyyy-MM-dd"));
          MailHelper.SendMail(User.Account.ACP_Mail, "Eurekaps tiene un mensaje para ti", "IdeaReg", valores);



          return RedirectToAction("Index");

        }
      }
      catch (DbEntityValidationException e)
      {
        string ex = "";
        foreach (var eve in e.EntityValidationErrors)
        {
          ex += ("Entity of type \""+ eve.Entry.Entity.GetType().Name+"\" in state \""+ eve.Entry.State+"\" has the following validation errors:");
          foreach (var ve in eve.ValidationErrors)
          {
            ex += ("- Property: \""+ ve.PropertyName + "\", Error: \""+ ve.ErrorMessage+"\"");
          }
        }
        throw new Exception(ex);
      }
      return View(model);
    }

    // GET: Idea/Edit/5
    public ActionResult EditIdea(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideaprofile eur_ideaprofile = db.eur_ideaprofile.Find(id);
      if (!Utilshelper.IsMyIdea(eur_ideaprofile))
      {
        throw new Exception("Alguien intento modificar la idea de: " + eur_ideaprofile.IDP_AddUser);
      }
      ViewBag.Archivo = db.eur_ideadocument.Where(a => a.ID_IDPID == id && a.ID_Type == "S").ToList();
      if (eur_ideaprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideaprofile);
    }
    [HttpPost]
    public ActionResult EditIdea(IdeaViewModel model)
    {
      
      if (ModelState.IsValid)
      {
        eur_ideaprofile idea = db.eur_ideaprofile.Where(a => a.IDP_AutoID == model.IDP_AutoID).FirstOrDefault();
        if (!Utilshelper.IsMyIdea(idea))
        {
          throw new Exception("Alguien intento modificar la idea de: " + idea.IDP_AddUser);
        }

        idea.IDP_APID = model.IDP_APID;
        idea.IDP_Comment = model.IDP_Comment;
        idea.IDP_Description = model.IDP_Description;
        idea.IDP_EPID = model.IDP_EPID;
        idea.IDP_ModifyDate = DateTime.Now;
        idea.IDP_ModifyUser = User.Identity.Name;
        idea.IDP_Name = model.IDP_Name;

        db.Entry(idea).State = EntityState.Modified;
        db.SaveChanges();

        if (model.Upload != null)
        {
          if (model.Upload.ContentLength > 0)
          {
           
              DirectoryInfo dir = new DirectoryInfo(Server.MapPath("~/Documentos/Idea/" + idea.IDP_AutoID));
              if (!dir.Exists)
              {
                dir.Create();
              }
              string path = Path.Combine(dir.ToString(), Path.GetFileName(model.Upload.FileName));
              eur_ideadocument doc = new eur_ideadocument
              {
                ID_AddDate = DateTime.Now,
                ID_AddUser = User.Identity.Name,
                ID_EditDate = DateTime.Now,
                ID_EditUser = User.Identity.Name,
                ID_IDPID = idea.IDP_AutoID,
                ID_Path = "Idea/" + idea.IDP_AutoID + "/" + model.Upload.FileName,
                ID_State = "A",
                ID_Type = "S"
              };
              db.eur_ideadocument.Add(doc);
              db.SaveChanges();
              model.Upload.SaveAs(path);
            
          }
        }
        return RedirectToAction("Index");

      }
      return View(model);
    }
    [HttpPost]
    public ActionResult SendMail(int id)
    {
      var model = db.eur_ideaprofile.Find(id);

      Dictionary<string, string> valores = new Dictionary<string, string>();
      valores.Add("Nombre", User.Account.ACP_FirstName);
      valores.Add("IdeaName", model.IDP_Name);
      valores.Add("CampainName", model.IDP_CampainName);
      valores.Add("FechaCierre", model.IDP_CierrerIniciativa.ToString("yyyy-MM-dd"));
      MailHelper.SendMail(User.Account.ACP_Mail, "Eurekaps tiene un mensaje para ti", "IniciativaReg", valores);
      return new JsonResult { Data = new { status = true } };
    }
    public ActionResult EditIniciativa(int? id)
    {
      var model = db.eur_ideaprofile.Find(id);
      if (!Utilshelper.IsMyIdea(model))
      {
        throw new Exception("Alguien intento modificar la iniciativa de: " + model.IDP_AddUser);
      }
      ViewBag.Archivo = db.eur_ideadocument.Where(a => a.ID_IDPID == id && a.ID_Type == "S").ToList();
      ViewBag.Colaboradores = db.eur_ideapartner.Where(a => a.IP_IDPID == id).ToList();
      ViewBag.Padrinos = db.eur_ideasponsor.Where(a => a.IS_IDPID == id).ToList();
      ViewBag.Indicadores = db.eur_ideaindicators.Where(a => a.II_IDPID == id).ToList();
      ViewBag.Validacion = db.eur_ideadocument.Where(a => a.ID_IDPID == id && a.ID_Type == "V").ToList();
      model.IDP_ModifyDate = DateTime.Now;
      model.IDP_ModifyUser = User.Identity.Name;
      model.IDP_Status = "IN";
      if(string.IsNullOrEmpty(model.IDP_IniciativaDate))
      {
        model.IDP_IniciativaDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm");
      }
      db.Entry(model).State = EntityState.Modified;
      db.SaveChanges();
      return View(model);
    }
    [HttpPost]
    public ActionResult EditIniciativa(eur_ideaprofile model)
    {
      ViewBag.Archivo = db.eur_ideadocument.Where(a => a.ID_IDPID == model.IDP_AutoID && a.ID_Type == "S").ToList();
      ViewBag.Colaboradores = db.eur_ideapartner.Where(a => a.IP_IDPID == model.IDP_AutoID).ToList();
      ViewBag.Padrinos = db.eur_ideasponsor.Where(a => a.IS_IDPID == model.IDP_AutoID).ToList();
      ViewBag.Indicadores = db.eur_ideaindicators.Where(a => a.II_IDPID == model.IDP_AutoID).ToList();
      ViewBag.Validacion = db.eur_ideadocument.Where(a => a.ID_IDPID == model.IDP_AutoID && a.ID_Type == "V").ToList();
      if (!Utilshelper.IsMyIdea(model))
      {
        throw new Exception("Alguien intento modificar la iniciativa de: " + model.IDP_AddUser);
      }

      if (ModelState.IsValid)
      {
        model.IDP_ModifyDate = DateTime.Now;
        model.IDP_ModifyUser = User.Identity.Name;
        model.IDP_Status = "IN";
       
        db.Entry(model).State = EntityState.Modified;
        try
        {
          db.SaveChanges();
        }
        catch (DbEntityValidationException e)
        {
          throw;
        }
      }
      return View(model);
    }
    [HttpPost]
    public ActionResult EditIniciativaJson(eur_ideaprofile model)
    {
      if (!Utilshelper.IsMyIdea(model))
      {
        throw new Exception("Alguien intento modificar la iniciativa de: " + model.IDP_AddUser);
  
      }

      var success = false;
      if (ModelState.IsValid)
      {
        model.IDP_ModifyDate = DateTime.Now;
        model.IDP_ModifyUser = User.Identity.Name;
        model.IDP_Status = "IN";
        db.Entry(model).State = EntityState.Modified;
        try
        {
          db.SaveChanges();
          success = true;
        }
        catch (DbEntityValidationException e)
        {
          throw;
        }
      }
      return new JsonResult { Data = new { status = success } };
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
