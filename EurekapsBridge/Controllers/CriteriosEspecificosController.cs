﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
  [PrixmaAuthorize]

  public class CriteriosEspecificosController : Controller
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: CriteriosEspecificos
    public ActionResult Index()
    {
      return View(db.eur_campaincriteria.ToList());
    }

    // GET: CriteriosEspecificos/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campaincriteria eur_campaincriteria = db.eur_campaincriteria.Find(id);
      if (eur_campaincriteria == null)
      {
        return HttpNotFound();
      }
      return View(eur_campaincriteria);
    }

    // GET: CriteriosEspecificos/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: CriteriosEspecificos/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "CMC_AutoID,CMC_CampainID,CMC_Name,CMC_Description,CMC_Type,CMC_State,CMC_AddDate,CMC_AddUser,CMC_EditDate,CMC_EditUser")] eur_campaincriteria eur_campaincriteria)
    {
      if (ModelState.IsValid)
      {
        db.eur_campaincriteria.Add(eur_campaincriteria);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_campaincriteria);
    }

    [HttpPost]
    public JsonResult CreateVM(string Name, string Description,string Type,string campain)
    {
      var status = true;
      var ent = new eur_campaincriteria()
      {
        CMC_AddDate = DateTime.Now,
        CMC_AddUser = User.Identity.Name,
        CMC_CampainID = campain,
        CMC_Description = Description,
        CMC_EditDate = DateTime.Now,
        CMC_EditUser = User.Identity.Name,
        CMC_Name = Name,
        CMC_State = "A",
        CMC_Type = Type
      };
      db.eur_campaincriteria.Add(ent);
      db.SaveChanges();
      db.Entry(ent).GetDatabaseValues();
      return new JsonResult { Data = new { status = status, id=ent.CMC_AutoID } };
    }
    // GET: CriteriosEspecificos/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campaincriteria eur_campaincriteria = db.eur_campaincriteria.Find(id);
      if (eur_campaincriteria == null)
      {
        return HttpNotFound();
      }
      return View(eur_campaincriteria);
    }

    // POST: CriteriosEspecificos/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "CMC_AutoID,CMC_CampainID,CMC_Name,CMC_Description,CMC_Type,CMC_State,CMC_AddDate,CMC_AddUser,CMC_EditDate,CMC_EditUser")] eur_campaincriteria eur_campaincriteria)
    {
      if (ModelState.IsValid)
      {
        db.Entry(eur_campaincriteria).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_campaincriteria);
    }

    // GET: CriteriosEspecificos/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campaincriteria eur_campaincriteria = db.eur_campaincriteria.Find(id);
      if (eur_campaincriteria == null)
      {
        return HttpNotFound();
      }
      return View(eur_campaincriteria);
    }

    // POST: CriteriosEspecificos/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_campaincriteria eur_campaincriteria = db.eur_campaincriteria.Find(id);
      db.eur_campaincriteria.Remove(eur_campaincriteria);
      db.SaveChanges();
      return RedirectToAction("Index");
    }
    [HttpPost]
    public ActionResult DeleteVM(int id)
    {
      eur_campaincriteria eur_campaincriteria = db.eur_campaincriteria.Find(id);
      db.eur_campaincriteria.Remove(eur_campaincriteria);
      db.SaveChanges();
      return new JsonResult { Data = new { status = true } };
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
