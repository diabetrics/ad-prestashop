﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
    [PrixmaAuthorize]
  public class EmpresasController : BaseController
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: Empresas
    public ActionResult Index()
    {
      return View(db.eur_enterpriseprofile.ToList());
    }

    // GET: Empresas/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_enterpriseprofile eur_enterpriseprofile = db.eur_enterpriseprofile.Find(id);
      if (eur_enterpriseprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_enterpriseprofile);
    }

    // GET: Empresas/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Empresas/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "EP_AutoID,EP_Name,EP_Description,EP_State,EP_AddDate,EP_AddUser,EP_EditDate,EP_EditUser")] eur_enterpriseprofile eur_enterpriseprofile)
    {
      if (ModelState.IsValid)
      {
        eur_enterpriseprofile.EP_AddDate = DateTime.Now;
        eur_enterpriseprofile.EP_AddUser = User.Account.ACP_User;
        eur_enterpriseprofile.EP_EditDate = DateTime.Now;
        eur_enterpriseprofile.EP_EditUser = User.Account.ACP_User;

        db.eur_enterpriseprofile.Add(eur_enterpriseprofile);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_enterpriseprofile);
    }

    // GET: Empresas/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_enterpriseprofile eur_enterpriseprofile = db.eur_enterpriseprofile.Find(id);
      if (eur_enterpriseprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_enterpriseprofile);
    }

    // POST: Empresas/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "EP_AutoID,EP_Name,EP_Description,EP_State,EP_AddDate,EP_AddUser,EP_EditDate,EP_EditUser")] eur_enterpriseprofile eur_enterpriseprofile)
    {
      if (ModelState.IsValid)
      {
        eur_enterpriseprofile.EP_EditDate = DateTime.Now;
        eur_enterpriseprofile.EP_EditUser = User.Account.ACP_User;
        db.Entry(eur_enterpriseprofile).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_enterpriseprofile);
    }

    // GET: Empresas/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_enterpriseprofile eur_enterpriseprofile = db.eur_enterpriseprofile.Find(id);
      if (eur_enterpriseprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_enterpriseprofile);
    }

    // POST: Empresas/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_enterpriseprofile eur_enterpriseprofile = db.eur_enterpriseprofile.Find(id);
      db.eur_enterpriseprofile.Remove(eur_enterpriseprofile);
      db.SaveChanges();
      return RedirectToAction("Index");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
