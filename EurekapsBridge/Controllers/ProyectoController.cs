﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
    public class ProyectoController : BaseController
    {
        private InngeniaEntities db = new InngeniaEntities();

        // GET: Projecto
        public ActionResult Index()
        {
            return View(db.eur_projectprofile.Where(a=> a.PP_Leader == User.Identity.Name).ToList());
        }

        // GET: Projecto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_projectprofile eur_projectprofile = db.eur_projectprofile.Find(id);
            if (eur_projectprofile == null)
            {
                return HttpNotFound();
            }
            return View(eur_projectprofile);
        }
    [PrixmaAuthorize]
    public ActionResult AsignarLider(int? id, int? cAutoId)
    {
      if (id == null)
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      if (db.eur_ideaprofile.Find(id) == null)
        return HttpNotFound();
      
      var ent = new eur_projectprofile { PP_IDPID = id.Value };
      if (cAutoId == null)
        ViewBag.cAutoId = (from d in db.eur_ideaprofile
                           join c in db.eur_campainprofile on d.IDP_CampainID equals c.CAM_CampainID
                           select c.CAM_AutoID).FirstOrDefault();
      else
        ViewBag.cAutoId = cAutoId.Value;
      return View(ent);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult AsignarLider(eur_projectprofile model)
    {
      if (ModelState.IsValid)
      {
        model.PP_AddDate = DateTime.Now;
        model.PP_AddUser = User.Identity.Name;
        model.PP_ModifyDate = DateTime.Now;
        model.PP_ModifyUser = User.Identity.Name;
        db.eur_projectprofile.Add(model);
        var idea = db.eur_ideaprofile.Where(a => a.IDP_AutoID == model.PP_IDPID).FirstOrDefault();
        var camid = (from  c in db.eur_campainprofile
                     where c.CAM_CampainID == idea.IDP_CampainID
                     select c.CAM_AutoID).FirstOrDefault();
        idea.IDP_Status = "P";
        db.Entry(idea).State = EntityState.Modified;
        db.SaveChanges();

        return RedirectToAction("Details", "Campana", new { id = camid });
      }

      return View(model);
    }
    // GET: Projecto/Create
    public ActionResult Create(int? id)
    {
      if (id == null)
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      if (db.eur_ideaprofile.Find(id) == null)
        return HttpNotFound();
      var ent = new eur_projectprofile { PP_IDPID = id.Value };
      return View(ent);
    }

        // POST: Projecto/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PP_AutoID,PP_IDPID,PP_EDT,PP_InitDate,PP_FinishDate,PP_Investment,PP_Expenses,PP_FinalcialEval,PP_Leader,PP_AddDate,PP_AddUser,PP_ModifyDate,PP_ModifyUser")] eur_projectprofile eur_projectprofile)
        {
            if (ModelState.IsValid)
            {
                db.eur_projectprofile.Add(eur_projectprofile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eur_projectprofile);
        }

        // GET: Projecto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_projectprofile eur_projectprofile = db.eur_projectprofile.Find(id);
            if (eur_projectprofile == null)
            {
                return HttpNotFound();
            }
            return View(eur_projectprofile);
        }

    // POST: Projecto/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "PP_AutoID,PP_IDPID,PP_EDT,PP_InitDate,PP_FinishDate,PP_Investment,PP_Expenses,PP_FinalcialEval,PP_Leader,PP_AddDate,PP_AddUser,PP_ModifyDate,PP_ModifyUser")] eur_projectprofile model)
    {
      if (ModelState.IsValid)
      {
        model.PP_ModifyDate = DateTime.Now;
        model.PP_ModifyUser = User.Identity.Name;
        db.Entry(model).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      
      return View(model);
    }

        // GET: Projecto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_projectprofile eur_projectprofile = db.eur_projectprofile.Find(id);
            if (eur_projectprofile == null)
            {
                return HttpNotFound();
            }
            return View(eur_projectprofile);
        }

        // POST: Projecto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eur_projectprofile eur_projectprofile = db.eur_projectprofile.Find(id);
            db.eur_projectprofile.Remove(eur_projectprofile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
