﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
    [PrixmaAuthorize]
  [Authorize]
    public class TenicoController : BaseController
    {
        private InngeniaEntities db = new InngeniaEntities();

        // GET: Tenico
        public ActionResult Index()
        {
            return View(db.eur_exception.ToList());
        }

        // GET: Tenico/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_exception eur_exception = db.eur_exception.Find(id);
            if (eur_exception == null)
            {
                return HttpNotFound();
            }
            return View(eur_exception);
        }

        // GET: Tenico/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tenico/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EXP_AutoID,EXP_Controller,EXP_Action,EXP_Message,EXP_Stack,EXP_InnerMessage,EXP_AddUser,EXP_AddDate,EXP_ModifyUser,EXP_ModifyDate")] eur_exception eur_exception)
        {
            if (ModelState.IsValid)
            {
                db.eur_exception.Add(eur_exception);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eur_exception);
        }

        // GET: Tenico/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_exception eur_exception = db.eur_exception.Find(id);
            if (eur_exception == null)
            {
                return HttpNotFound();
            }
            return View(eur_exception);
        }

        // POST: Tenico/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EXP_AutoID,EXP_Controller,EXP_Action,EXP_Message,EXP_Stack,EXP_InnerMessage,EXP_AddUser,EXP_AddDate,EXP_ModifyUser,EXP_ModifyDate")] eur_exception eur_exception)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eur_exception).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eur_exception);
        }

        // GET: Tenico/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eur_exception eur_exception = db.eur_exception.Find(id);
            if (eur_exception == null)
            {
                return HttpNotFound();
            }
            return View(eur_exception);
        }

        // POST: Tenico/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eur_exception eur_exception = db.eur_exception.Find(id);
            db.eur_exception.Remove(eur_exception);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
