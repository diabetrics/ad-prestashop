﻿using EurekapsBridge.Code.Helpers;
using EurekapsBridge.Code.Security;
using EurekapsBridge.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace EurekapsBridge.Controllers
{
  public class AccountController : BaseController
  {
    InngeniaEntities db = new InngeniaEntities();
    // GET: Account
    public ActionResult Index()
    {
      if (User.Account.ACP_IsAdmin == "N")
        throw new Exception("Alguien intento entrar a un lugar que no debia.");
      return View(db.eur_account.Where(a => !string.IsNullOrEmpty(a.ACP_Password)));

    }
    [HttpPost]
    public JsonResult Get(string User)
    {
      var acc = db.eur_account.Where(a => a.ACP_User == User).FirstOrDefault();
      if (acc == null)
        if (Utils.IsDomainUser(User))
        {
          Utils.CreateInngeniaUser(User, out acc);
          long ShopId;
          if (!Utils.IsShopUser(acc.ACP_Mail, out ShopId))
            Utils.CreateShopUser(acc);

          return new JsonResult { Data = new { status = true, FullName = acc.ACP_FirstName + " " + acc.ACP_LastName } };
        }
        else
        {
          return new JsonResult { Data = new { status = false } };
        }
      else
        return new JsonResult { Data = new { status = true, FullName = acc.ACP_FirstName + " " + acc.ACP_LastName } };
    }

    [AllowAnonymous]
    public ActionResult Create(string ReturnUrl)
    {
      ViewBag.ReturnUrl = ReturnUrl;
      return View();
    }
    [AllowAnonymous, HttpPost]
    public ActionResult Create(CreateAccountViewModel model, string ReturnUrl)
    {

      if (ModelState.IsValid)
      {
        if (Utils.IsInngeniaUser(model.ACP_User))
        {
          ViewBag.Error = "El usuario que intenta crear ya esta registrado en el sistema.";
          ViewBag.ReturnUrl = ReturnUrl;
          return View();
        }
        eur_account acc = new eur_account
        {
          ACP_AddDate = DateTime.Now,
          ACP_FirstName = model.ACP_FirstName,
          ACP_ID = model.ACP_User,
          ACP_IsAdmin = "N",
          ACP_LastLoginDate = DateTime.Now,
          ACP_User = model.ACP_User,
          ACP_LastName = model.ACP_LastName,
          ACP_Mail = model.ACP_Mail,
          ACP_Password = model.ACP_Password.ToMD5(),

        };
        db.eur_account.Add(acc);
        db.SaveChanges();
        long idShop = 0;
        if (!Utils.IsShopUser(acc.ACP_Mail, out idShop))
          Utils.CreateShopUser(acc);
        Utils.AsignarGrupoShop(idShop);
        string userData = JsonConvert.SerializeObject(acc);
        FormsAuthenticationTicket auth = new FormsAuthenticationTicket(
            1, model.ACP_User, DateTime.Now, DateTime.Now.AddMinutes(15), false, userData
            );

        string enct = FormsAuthentication.Encrypt(auth);

        HttpCookie fa = new HttpCookie(FormsAuthentication.FormsCookieName, enct);

        Response.Cookies.Add(fa);
        if (!string.IsNullOrEmpty(ReturnUrl))
          return Redirect(ReturnUrl);
        return RedirectToAction("Index");
      }
      return View();
    }

    public ActionResult Edit(int? id)
    {
      if (User.Account.ACP_IsAdmin == "N")
        throw new Exception("Alguien intento entrar a un lugar que no debia.");
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_account ent = db.eur_account.Where(a => a.ACP_AutoID == id).FirstOrDefault();
      if (ent == null)
        return HttpNotFound();
      return View(ent);
    }
    public ActionResult CambiarPass(int? id)
    {

      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_account ent = db.eur_account.Where(a => a.ACP_AutoID == id).FirstOrDefault();
      if (string.IsNullOrEmpty(ent.ACP_Password))
        throw new Exception("Este usuario no puede cambiar su contraseña");
      ChangepasswordViewModel cpent = new ChangepasswordViewModel
      {
        ACP_AutoID = ent.ACP_AutoID
      };
      if (ent == null)
        return HttpNotFound();
      if (User.Account.ACP_AutoID != ent.ACP_AutoID)
        throw new Exception("Alguien intento cambiar la contraseña de un usuario que no era el suyo.");
      return View(cpent);
    }
    [HttpPost]
    public ActionResult CambiarPass(ChangepasswordViewModel ent)
    {
      if (ModelState.IsValid)
      {
        var entidad = db.eur_account.Where(a => a.ACP_AutoID == ent.ACP_AutoID).FirstOrDefault();
        if (entidad == null)
          return HttpNotFound();
        entidad.ACP_Password = ent.NewPassword.ToMD5();
        db.Entry(entidad).State = System.Data.Entity.EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index", "Home");
      }
      return View(ent);
    }
    [HttpPost]
    public ActionResult Edit(eur_account ent)
    {
      if (ModelState.IsValid)
      {
        if (string.IsNullOrEmpty(ent.ACP_Password))
          using(var db_ = new InngeniaEntities())
          {
            ent.ACP_Password = db_.eur_account.Where(a => a.ACP_AutoID == ent.ACP_AutoID).First().ACP_Password;
          }
        else
          ent.ACP_Password = ent.ACP_Password.ToMD5();
        db.Entry(ent).State = System.Data.Entity.EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(ent);
    }
    [HttpPost]
    public ActionResult restorePass(RestoreAccountViewModel model)
    {
      if (ModelState.IsValid)
      {
        var user = db.eur_account.Where(a => a.ACP_User == model.ACP_User && a.ACP_Mail == model.ACP_Mail && !string.IsNullOrEmpty(a.ACP_Password)).FirstOrDefault();
        if (user == null)
          ViewBag.Error = "No se puede realizar el cambio de contraseña con los datos proporcionados";
        else
        {
          user.ACP_Password = RandomString(8).ToMD5(); 
          ViewBag.Message = "";
        }
      }
      return View();
    }
    private static Random random = new Random();
    public static string RandomString(int length)
    {
      const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      return new string(Enumerable.Repeat(chars, length)
        .Select(s => s[random.Next(s.Length)]).ToArray());
    }
  }
}