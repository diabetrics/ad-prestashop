﻿using EurekapsBridge.Code.Helpers;
using EurekapsBridge.Code.Security;
using EurekapsBridge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EurekapsBridge.Controllers
{
    [PrixmaAuthorize]
  public class EvaluacionController : Controller
  {
    private InngeniaEntities db = new InngeniaEntities();
    // GET: Evaluacion
    public ActionResult Index()
    {
      return View();
    }
    public ActionResult Evaluar(int? id)
    {
      if (id == null)
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      eur_ideaprofile ent = db.eur_ideaprofile.Find(id);
      ViewBag.Criterios = BuscarCriteriosEspecificos(id.Value, ent.IDP_CampainID);
      ViewBag.Generales = BuscarCriteriosGenerales(id.Value, ent.IDP_CampainID);
      ViewBag.Requerimientos = BuscarCriteriosRequerimientos(id.Value, ent.IDP_CampainID);

      ViewBag.Archivo = db.eur_ideadocument.Where(a => a.ID_IDPID == id && a.ID_Type == "S").ToList();
      ViewBag.Colaboradores = db.eur_ideapartner.Where(a => a.IP_IDPID == id).ToList();
      ViewBag.Padrinos = db.eur_ideasponsor.Where(a => a.IS_IDPID == id).ToList();
      ViewBag.Indicadores = db.eur_ideaindicators.Where(a => a.II_IDPID == id).ToList();
      ViewBag.Validacion = db.eur_ideadocument.Where(a => a.ID_IDPID == id && a.ID_Type == "V").ToList();
      ViewBag.Campaña = db.eur_campainprofile.Where(a => a.CAM_CampainID == ent.IDP_CampainID).First();
      ViewBag.Feedback = db.eur_ideafeedbak.Where(a => a.IFB_IDPID == id).ToList();
      return View(ent);
    }
    [HttpPost]
    public ActionResult Evaluar(FormCollection frm)
    {
      var form = Request.Form;
      int id = int.Parse(form["IDP_AutoID"]);
      eur_ideaprofile ent = db.eur_ideaprofile.Find(id);
      eur_account acc = db.eur_account.Where(a => a.ACP_User == ent.IDP_AddUser).First();

      var generales = Request.Form.AllKeys.Where(a => a.Contains("G_"));
      foreach (var str in generales)
      {
        string val = frm[str]; int valor = 0;
        if (!int.TryParse(val, out valor))
          valor = 0;
        var idgen = int.Parse(str.Remove(0, 2));
          var genup = db.eur_evalgeneral.Where(a => a.CEG_CampainID == ent.IDP_CampainID && a.CEG_IDPID == ent.IDP_AutoID && a.CEG_GeneralID == idgen).FirstOrDefault();
          if(genup == null)
          {
            eur_evalgeneral gen = new eur_evalgeneral
            {

              CEG_AddDate = DateTime.Now,
              CEG_AddUser = User.Identity.Name,
              CEG_CampainID = ent.IDP_CampainID,
              CEG_EditDate = DateTime.Now,
              CEG_EditUser = User.Identity.Name,
              CEG_IDPID = ent.IDP_AutoID,
              CEG_Value = valor,
              CEG_GeneralID = int.Parse(str.Remove(0, 2))
            };
            db.eur_evalgeneral.Add(gen);
          }
          else
          {
            genup.CEG_EditDate = DateTime.Now;
            genup.CEG_EditUser = User.Identity.Name;
            genup.CEG_Value = valor;
            db.Entry(genup).State = System.Data.Entity.EntityState.Modified;
          }

        
        
      }
      var criterios = Request.Form.AllKeys.Where(a => a.Contains("C_"));
      foreach (var str in criterios)
      {
        string val = frm[str]; int valor = 0;
        if (!int.TryParse(val, out valor))
          valor = 0;
        var idcri = int.Parse(str.Remove(0, 2));
        var criupd = db.eur_evalcriteria.Where(a => a.EC_CampainID == ent.IDP_CampainID && a.EC_IDPID == ent.IDP_AutoID && a.EC_CriteriaID == idcri).FirstOrDefault();
        if (criupd == null)
        {
          eur_evalcriteria gen = new eur_evalcriteria
          {
            EC_AddDate = DateTime.Now,
            EC_AddUser = User.Identity.Name,
            EC_CampainID = ent.IDP_CampainID,
            EC_EditDate = DateTime.Now,
            EC_EditUser = User.Identity.Name,
            EC_IDPID = ent.IDP_AutoID,
            EC_Value = valor,
            EC_CriteriaID = int.Parse(str.Remove(0, 2))
          };
          db.eur_evalcriteria.Add(gen);
        }
        else
        {
          criupd.EC_EditDate = DateTime.Now;
          criupd.EC_EditUser = User.Identity.Name;
          criupd.EC_Value = valor;
          db.Entry(criupd).State = System.Data.Entity.EntityState.Modified;
        }
      }
      var Req = Request.Form.AllKeys.Where(a => a.Contains("R_"));
      foreach (var str in Req)
      {
        string val = frm[str]; int valor = 0;
        if (!int.TryParse(val, out valor))
          valor = 0;
        var idreq = int.Parse(str.Remove(0, 2));

        var requpd = db.eur_evalreq.Where(a => a.CER_CampainID == ent.IDP_CampainID && a.CER_IDPID == ent.IDP_AutoID && a.CER_ReqID == idreq).FirstOrDefault();
        if (requpd == null)
        {
          eur_evalreq gen = new eur_evalreq
          {
            CER_AddDate = DateTime.Now,
            CER_AddUser = User.Identity.Name,
            CER_CampainID = ent.IDP_CampainID,
            CER_EditDate = DateTime.Now,
            CER_EditUser = User.Identity.Name,
            CER_IDPID = ent.IDP_AutoID,
            CER_Value = valor,
            CER_ReqID = int.Parse(str.Remove(0, 2))
          };
          db.eur_evalreq.Add(gen);
        }
        else
        {
          requpd.CER_EditDate = DateTime.Now;
          requpd.CER_EditUser = User.Identity.Name;
          requpd.CER_Value = valor;
        }
        
      }
      ent.IDP_Status = "E";
      string fb = Request.Form["FeedBack"];
      if (!string.IsNullOrEmpty(fb))
      {
        ent.IDP_Status = "EF";
        eur_ideafeedbak ifb = new eur_ideafeedbak
        {
           IFB_AddDate = DateTime.Now,
           IFB_AddUser = User.Identity.Name,
           IFB_IDPID = ent.IDP_AutoID,
           IFB_Mensaje = fb,
           IFB_ModifyDate = DateTime.Now,
           IFB_ModifyUser = User.Identity.Name
        };
        db.eur_ideafeedbak.Add(ifb);
        Dictionary<string, string> valores = new Dictionary<string, string>();
        valores.Add("Nombre", acc.ACP_FirstName);
        valores.Add("IdeaName", ent.IDP_Name);
        valores.Add("CampainName", ent.IDP_CampainName);
        valores.Add("Recomendaciones", fb);
        MailHelper.SendMail(acc.ACP_Mail, "Eurekaps tiene un mensaje para ti", "feedback", valores);
      }
      try
      {
       
        db.Entry(ent).State = System.Data.Entity.EntityState.Modified;
        db.SaveChanges();
      }
      catch (Exception)
      {

        ViewBag.Criterios = BuscarCriteriosEspecificos(ent.IDP_AutoID, ent.IDP_CampainID);
        ViewBag.Generales = BuscarCriteriosGenerales(ent.IDP_AutoID, ent.IDP_CampainID);
        ViewBag.Requerimientos = BuscarCriteriosRequerimientos(ent.IDP_AutoID, ent.IDP_CampainID);

        ViewBag.Archivo = db.eur_ideadocument.Where(a => a.ID_IDPID == id && a.ID_Type == "S").ToList();
        ViewBag.Colaboradores = db.eur_ideapartner.Where(a => a.IP_IDPID == id).ToList();
        ViewBag.Padrinos = db.eur_ideasponsor.Where(a => a.IS_IDPID == id).ToList();
        ViewBag.Indicadores = db.eur_ideaindicators.Where(a => a.II_IDPID == id).ToList();
        ViewBag.Validacion = db.eur_ideadocument.Where(a => a.ID_IDPID == id && a.ID_Type == "V").ToList();
        ViewBag.Campaña = db.eur_campainprofile.Where(a => a.CAM_CampainID == ent.IDP_CampainID).First();
        return View(ent);
      }
      eur_campainprofile cam = db.eur_campainprofile.Where(a => a.CAM_CampainID == ent.IDP_CampainID).FirstOrDefault();
      return RedirectToAction("Details", "Campana", new { id = cam.CAM_AutoID });
    }
    private List<CriteriosViewModel> BuscarCriteriosGenerales(int IdeaID, string CampainID)
    {
      var datos = from g in db.eur_campaingeneral
                  where g.CMG_State == "A"
                  select g;
      var eval = from e in db.eur_evalgeneral
                 where e.CEG_IDPID == IdeaID
                 select e;
      return (from g in datos
              join e in eval on g.CMG_AutoID equals e.CEG_GeneralID into gj
              from sube in gj.DefaultIfEmpty()
              select new CriteriosViewModel
              {
                Desc = g.CMG_Description,
                id = g.CMG_AutoID,
                Name = g.CMG_Name,
                Type = g.CMG_Type,
                Value = sube.CEG_Value.ToString()
              }).ToList();
    }
    private List<CriteriosViewModel> BuscarCriteriosEspecificos(int IdeaID, string CampainID)
    {
      var datos = from g in db.eur_campaincriteria
                  where g.CMC_State == "A" && g.CMC_CampainID == CampainID
                  select g;
      var eval = from e in db.eur_evalcriteria
                 where e.EC_IDPID == IdeaID
                 select e;
      return (from g in datos
              join e in eval on g.CMC_AutoID equals e.EC_CriteriaID into gj
              from sube in gj.DefaultIfEmpty()
              select new CriteriosViewModel
              {
                Desc = g.CMC_Description,
                id = g.CMC_AutoID,
                Name = g.CMC_Name,
                Type = g.CMC_Type,
                Value = sube.EC_Value.ToString()
              }).ToList();
    }
    private List<CriteriosViewModel> BuscarCriteriosRequerimientos(int IdeaID, string CampainID)
    {
      var datos = from g in db.eur_campainreq
                  where g.CMR_State == "A" && g.CMR_CampainID == CampainID
                  select g;
      var eval = from e in db.eur_evalreq
                 where e.CER_IDPID == IdeaID
                 select e;
      return (from g in datos
              join e in eval on g.CMR_AutoID equals e.CER_ReqID into gj
              from sube in gj.DefaultIfEmpty()
              select new CriteriosViewModel
              {
                Desc = g.CMR_Description,
                id = g.CMR_AutoID,
                Name = g.CMR_Name,
                Type = g.CMR_Type,
                Value = sube.CER_Value.ToString()
              }).ToList();
    }
  }
}