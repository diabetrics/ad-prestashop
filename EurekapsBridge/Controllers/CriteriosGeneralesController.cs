﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
  [PrixmaAuthorize]

  public class CriteriosGeneralesController : BaseController
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: CriteriosGenerales
    public ActionResult Index()
    {
      return View(db.eur_campaingeneral.ToList());
    }

    // GET: CriteriosGenerales/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campaingeneral eur_campaingeneral = db.eur_campaingeneral.Find(id);
      if (eur_campaingeneral == null)
      {
        return HttpNotFound();
      }
      return View(eur_campaingeneral);
    }

    // GET: CriteriosGenerales/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: CriteriosGenerales/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "CMG_AutoID,CMG_CampainID,CMG_Name,CMG_Description,CMG_Type,CMG_State,CMG_AddDate,CMG_AddUser,CMG_EditDate,CMG_EditUser")] eur_campaingeneral eur_campaingeneral)
    {
      if (ModelState.IsValid)
      {
        eur_campaingeneral.CMG_AddDate = DateTime.Now;
        eur_campaingeneral.CMG_AddUser = User.Account.ACP_User;
        eur_campaingeneral.CMG_EditDate = DateTime.Now;
        eur_campaingeneral.CMG_EditUser = User.Account.ACP_User;

        db.eur_campaingeneral.Add(eur_campaingeneral);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_campaingeneral);
    }

    // GET: CriteriosGenerales/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campaingeneral eur_campaingeneral = db.eur_campaingeneral.Find(id);
      if (eur_campaingeneral == null)
      {
        return HttpNotFound();
      }
      return View(eur_campaingeneral);
    }

    // POST: CriteriosGenerales/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "CMG_AutoID,CMG_CampainID,CMG_Name,CMG_Description,CMG_Type,CMG_State,CMG_AddDate,CMG_AddUser,CMG_EditDate,CMG_EditUser")] eur_campaingeneral eur_campaingeneral)
    {
      if (ModelState.IsValid)
      {
        eur_campaingeneral.CMG_EditDate = DateTime.Now;
        eur_campaingeneral.CMG_EditUser = User.Account.ACP_User;

        db.Entry(eur_campaingeneral).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_campaingeneral);
    }

    // GET: CriteriosGenerales/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campaingeneral eur_campaingeneral = db.eur_campaingeneral.Find(id);
      if (eur_campaingeneral == null)
      {
        return HttpNotFound();
      }
      return View(eur_campaingeneral);
    }

    // POST: CriteriosGenerales/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_campaingeneral eur_campaingeneral = db.eur_campaingeneral.Find(id);
      db.eur_campaingeneral.Remove(eur_campaingeneral);
      db.SaveChanges();
      return RedirectToAction("Index");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
