﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;

namespace EurekapsBridge.Controllers
{
  public class AreaController : BaseController
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: Area
    public ActionResult Index()
    {
      return View(db.eur_areaprofile.ToList());
    }

    // GET: Area/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_areaprofile eur_areaprofile = db.eur_areaprofile.Find(id);
      if (eur_areaprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_areaprofile);
    }

    // GET: Area/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Area/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "AP_AutoID,AP_Name,AP_Description,AP_State,AP_AddDate,AP_AddUser,AP_EditDate,AP_EditUser")] eur_areaprofile eur_areaprofile)
    {
      if (ModelState.IsValid)
      {
        eur_areaprofile.AP_AddDate = DateTime.Now;
        eur_areaprofile.AP_AddUser = User.Account.ACP_User;
        eur_areaprofile.AP_EditDate = DateTime.Now;
        eur_areaprofile.AP_EditUser = User.Account.ACP_User;

        db.eur_areaprofile.Add(eur_areaprofile);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_areaprofile);
    }

    // GET: Area/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_areaprofile eur_areaprofile = db.eur_areaprofile.Find(id);
      if (eur_areaprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_areaprofile);
    }

    // POST: Area/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "AP_AutoID,AP_Name,AP_Description,AP_State,AP_AddDate,AP_AddUser,AP_EditDate,AP_EditUser")] eur_areaprofile eur_areaprofile)
    {
      if (ModelState.IsValid)
      {
        eur_areaprofile.AP_EditDate = DateTime.Now;
        eur_areaprofile.AP_EditUser = User.Account.ACP_User;
        db.Entry(eur_areaprofile).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_areaprofile);
    }

    // GET: Area/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_areaprofile eur_areaprofile = db.eur_areaprofile.Find(id);
      if (eur_areaprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_areaprofile);
    }

    // POST: Area/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_areaprofile eur_areaprofile = db.eur_areaprofile.Find(id);
      db.eur_areaprofile.Remove(eur_areaprofile);
      db.SaveChanges();
      return RedirectToAction("Index");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
