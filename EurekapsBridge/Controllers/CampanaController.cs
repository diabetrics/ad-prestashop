﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;
using WordPressSharp.Models;

namespace EurekapsBridge.Controllers
{
  public class CampanaController : BaseController
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: Campana
    [PrixmaAuthorize]
    public ActionResult Index()
    {
      return View(db.eur_campainprofile.ToList());
    }

    // GET: Campana/Details/5
    [PrixmaAuthorize]
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campainprofile eur_campainprofile = db.eur_campainprofile.Find(id);
      if (eur_campainprofile == null)
      {
        return HttpNotFound();
      }
      ViewBag.Requerimientos = db.eur_campainreq.Where(a => a.CMR_CampainID == eur_campainprofile.CAM_CampainID && a.CMR_State == "A");
      ViewBag.Criterios = db.eur_campaincriteria.Where(a => a.CMC_CampainID == eur_campainprofile.CAM_CampainID && a.CMC_State == "A");
      ViewBag.Ideas = db.eur_ideaprofile.Where(a => a.IDP_CampainID == eur_campainprofile.CAM_CampainID && a.IDP_Status == "ID").OrderBy(a=> a.IDP_AddDate);
      ViewBag.Iniciativas = db.eur_ideaprofile.Where(a => a.IDP_CampainID == eur_campainprofile.CAM_CampainID && a.IDP_Status == "IN").OrderBy(a => a.IDP_IniciativaDate);
      var evaluadas =   db.eur_ideaprofile.Where(a => a.IDP_CampainID == eur_campainprofile.CAM_CampainID && a.IDP_Status == "E").OrderBy(a => a.IDP_IniciativaDate);
      var pendientes = db.eur_ideaprofile.Where(a => a.IDP_CampainID == eur_campainprofile.CAM_CampainID && a.IDP_Status == "EF").OrderBy(a => a.IDP_IniciativaDate);
      var datose = new List<eur_ideaprofile>();
      var datosp = new List<eur_ideaprofile>();
      foreach (var ent in evaluadas)
      {
        double gl, c, r;
        int glc, cc, rc;
        using (var db = new InngeniaEntities())
        {
          gl = (from e in db.eur_evalgeneral
                where e.CEG_IDPID == ent.IDP_AutoID
                group e by e.CEG_IDPID into g
                select g.Sum(a => a.CEG_Value)).FirstOrDefault();
          glc = (from e in db.eur_evalgeneral
                 where e.CEG_IDPID == ent.IDP_AutoID
                 group e by e.CEG_IDPID into g
                 select g.Count()).FirstOrDefault();
        }
        using (var db = new InngeniaEntities())
        {
          c = (from e in db.eur_evalcriteria
               where e.EC_IDPID == ent.IDP_AutoID
               group e by e.EC_IDPID into g
               select g.Sum(a => a.EC_Value)).FirstOrDefault();
          cc = (from e in db.eur_evalcriteria
                where e.EC_IDPID == ent.IDP_AutoID
                group e by e.EC_IDPID into g
                select g.Count()).FirstOrDefault();
        }
        using (var db = new InngeniaEntities())
        {
          r = (from e in db.eur_evalreq
               where e.CER_IDPID == ent.IDP_AutoID
               group e by e.CER_IDPID into g
               select g.Sum(a => a.CER_Value)).FirstOrDefault();
          rc = (from e in db.eur_evalreq
                where e.CER_IDPID == ent.IDP_AutoID
                group e by e.CER_IDPID into g
                select g.Count()).FirstOrDefault();
        }

        if (glc + cc + rc == 0)
          ent.IDP_TotalEval = 0;
        else
          ent.IDP_TotalEval = (gl + c + r) / (glc + cc + rc);
        datose.Add(ent);
      }

      ViewBag.Evaluadas = datose;

      foreach (var ent in pendientes)
      {
        double gl, c, r;
        int glc, cc, rc;
        using (var db = new InngeniaEntities())
        {
          gl = (from e in db.eur_evalgeneral
                where e.CEG_IDPID == ent.IDP_AutoID
                group e by e.CEG_IDPID into g
                select g.Sum(a => a.CEG_Value)).FirstOrDefault();
          glc = (from e in db.eur_campaingeneral
                
                select e).Count();
        }
        using (var db = new InngeniaEntities())
        {
          c = (from e in db.eur_evalcriteria
               where e.EC_IDPID == ent.IDP_AutoID
               group e by e.EC_IDPID into g
               select g.Sum(a => a.EC_Value)).FirstOrDefault();
          cc = (from e in db.eur_campaincriteria
               where e.CMC_CampainID == ent.IDP_CampainID
               select e).Count();
        }
        using (var db = new InngeniaEntities())
        {
          r = (from e in db.eur_evalreq
               where e.CER_IDPID == ent.IDP_AutoID
               group e by e.CER_IDPID into g
               select g.Sum(a => a.CER_Value)).FirstOrDefault();
          rc = (from e in db.eur_campainreq
               where e.CMR_CampainID == ent.IDP_CampainID
               select e).Count();
        }
        if (glc + cc + rc == 0)
          ent.IDP_TotalEval = 0;
        else
          ent.IDP_TotalEval = (gl + c + r) / (glc + cc + rc);
        datosp.Add(ent);
      }
      ViewBag.Pendientes = datosp;
      ViewBag.Proyectos = (from p in db.eur_projectprofile
                           join i in db.eur_ideaprofile on p.PP_IDPID equals i.IDP_AutoID
                           where i.IDP_CampainID == eur_campainprofile.CAM_CampainID
                           select p).ToList();
      return View(eur_campainprofile);
    }
    // GET: Campana/Details/5
    public ActionResult Detalles(string id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campainprofile eur_campainprofile = db.eur_campainprofile.Where(a=> a.CAM_CampainID == id).FirstOrDefault();
      ViewBag.Requerimientos = db.eur_campainreq.Where(a => a.CMR_CampainID == eur_campainprofile.CAM_CampainID && a.CMR_State == "A");
      ViewBag.Criterios = db.eur_campaincriteria.Where(a => a.CMC_CampainID == eur_campainprofile.CAM_CampainID && a.CMC_State == "A");
      if (eur_campainprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_campainprofile);
    }

    // GET: Campana/Create
    [PrixmaAuthorize]
    public ActionResult Create()
    {
      return View();
    }
    [PrixmaAuthorize]
        public ActionResult CreateVM()
    {
      CampainViewModel model = new CampainViewModel();
      model.CAM_GFechaIniciativas = DateTime.Now;
      model.CAM_FechaIdeas = DateTime.Now;
      return View(model);
    }
    [PrixmaAuthorize]
    [HttpPost]
    public JsonResult CreateVM(CampainViewModel model)
    {
      var status = false;
      var criterios = "";
      var requisitos = "";
      if (ModelState.IsValid)
      {
        eur_campainprofile camp = new eur_campainprofile()
        {
          CAM_AddDate = DateTime.Now,
          CAM_AddUser = User.Account.ACP_User,
          CAM_CampainID = model.CAM_CampainID,
          CAM_Description = model.CAM_Description,
          CAM_FechaIdea = model.CAM_FechaIdeas,
          CAM_FechaIniciativa = model.CAM_GFechaIniciativas,
          CAM_EditDate = DateTime.Now,
          CAM_EditUser = User.Account.ACP_User,
          CAM_Name = model.CAM_Name,
          CAM_State = model.CAM_State
        };
        db.eur_campainprofile.Add(camp);
        if(model.Requirimientos!= null)
        foreach(var req in model.Requirimientos)
        {
          eur_campainreq ent = new eur_campainreq()
          {
            CMR_AddDate = DateTime.Now,
            CMR_AddUser = User.Account.ACP_User,
            CMR_CampainID = model.CAM_CampainID,
            CMR_Description = req.Description,
            CMR_EditDate = DateTime.Now,
            CMR_EditUser = User.Account.ACP_User,
            CMR_Name = req.Name,
            CMR_State = req.State,
            CMR_Type = req.Type,
           
          };
            requisitos += "<li><b>" + ent.CMR_Name + "</b>: " + ent.CMR_Description + "</li>";

            db.eur_campainreq.Add(ent);
        }
        if (model.Criterios != null)
          foreach (var cri in model.Criterios)
          {
            eur_campaincriteria ent = new eur_campaincriteria()
            {
              CMC_AddDate = DateTime.Now,
              CMC_AddUser = User.Account.ACP_User,
              CMC_CampainID = model.CAM_CampainID,
              CMC_Description = cri.Description,
              CMC_EditDate = DateTime.Now,
              CMC_EditUser = User.Account.ACP_User,
              CMC_Name = cri.Name,
              CMC_State = cri.State,
              CMC_Type = cri.Type,

            };
            criterios += "<li><b>" + ent.CMC_Name + "</b>: " + ent.CMC_Description + "</li>";
            db.eur_campaincriteria.Add(ent);
          }
        db.SaveChanges();
        status = true;

        var contenido =
          " &nbsp; " +
          "<img class='img-responsive' src='http://eurekaps.procaps.com.co/wp-content/uploads/reduccion-costos-naturmega.jpg' /> " +
          "<table class='table table-condensed'> " +
          "    <thead> " +
          "        <tr> " +
          "            <th width='850'> " +
          "                <h2>Descripción de la campaña</h2> " +
          "                <p align='justify'>" + camp.CAM_Description + "</p> " +
          "                <h2>Criterios de evaluación</h2> " +
          "                &nbsp; " +
          "                <ul> " +
          criterios +
          "                </ul> " +
          "                <h2>Requisitos Especificos</h2> " +
          "                <ul> " +
          requisitos +
          "                </ul> " +
          "            </th> " +
          "            <th></th> " +
          "            <th style='vertical-align: text-top;' width='200'> " +
          "                <h2>IDEAS</h2> " +
          "                <h4>Si tienes una idea cuéntanosla aquí</h4> " +
          "                <a class='btn btn-success' href='http://appeurekaps.procaps.com.co/Idea/createidea?campainID=" + model.CAM_CampainID + "' type='button'>Registra tu idea</a> " +
          "            </th> " +
          "        </tr> " +
          "    </thead> " +
          "</table>";
        using ( var client = new WordPressSharp.WordPressClient())
        {
          var page = new Post
          {
            PostType = "page",
            Title = camp.CAM_Name,
            Content =contenido,
            PublishDateTime = DateTime.Now,
            Status="publish"
          };
          var id = client.NewPost(page);
        }
      }
      return new JsonResult { Data = new { status = status } };
    }

    // POST: Campana/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [PrixmaAuthorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "CAM_AutoID,CAM_CampainID,CAM_Name,CAM_Description,CAM_State,CAM_AddDate,CAM_AddUser,CAM_EditDate,CAM_EditUser")] eur_campainprofile eur_campainprofile)
    {
      if (ModelState.IsValid)
      {
        eur_campainprofile.CAM_AddDate = DateTime.Now;
        eur_campainprofile.CAM_AddUser = User.Account.ACP_User;
        eur_campainprofile.CAM_EditDate = DateTime.Now;
        eur_campainprofile.CAM_EditUser = User.Account.ACP_User;

        db.eur_campainprofile.Add(eur_campainprofile);
        db.SaveChanges();
        db.Entry(eur_campainprofile).GetDatabaseValues();
        return RedirectToAction("Edit", new { id = eur_campainprofile.CAM_AutoID });
      }

      return View(eur_campainprofile);
    }

    [PrixmaAuthorize]
    // GET: Campana/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campainprofile eur_campainprofile = db.eur_campainprofile.Find(id);
      var criterios = db.eur_campaincriteria.Where(a => a.CMC_CampainID == eur_campainprofile.CAM_CampainID);
      ViewBag.Criterios = criterios;

      var requerimientos = db.eur_campainreq.Where(a => a.CMR_CampainID ==eur_campainprofile.CAM_CampainID);
      ViewBag.Requerimientos = requerimientos;
      if (eur_campainprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_campainprofile);
    }

  
    // POST: Campana/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [PrixmaAuthorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(eur_campainprofile eur_campainprofile)
    {
      if (ModelState.IsValid)
      {
        eur_campainprofile.CAM_EditDate = DateTime.Now;
        eur_campainprofile.CAM_EditUser = User.Account.ACP_User;

        db.Entry(eur_campainprofile).State = EntityState.Modified;
       
          db.SaveChanges();
       
        return RedirectToAction("Index");
      }
      return View(eur_campainprofile);
    }

    // GET: Campana/Delete/5
    [PrixmaAuthorize]
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_campainprofile eur_campainprofile = db.eur_campainprofile.Find(id);
      if (eur_campainprofile == null)
      {
        return HttpNotFound();
      }
      return View(eur_campainprofile);
    }

    // POST: Campana/Delete/5
    [PrixmaAuthorize]
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_campainprofile eur_campainprofile = db.eur_campainprofile.Find(id);
      db.eur_campainprofile.Remove(eur_campainprofile);
      db.SaveChanges();
      return RedirectToAction("Index");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
