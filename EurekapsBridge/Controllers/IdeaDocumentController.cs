﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using System.IO;

namespace EurekapsBridge.Controllers
{
  public class IdeaDocumentController : Controller
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: IdeaDocument
    public ActionResult Index()
    {
      return View(db.eur_ideadocument.ToList());
    }

    // GET: IdeaDocument/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideadocument eur_ideadocument = db.eur_ideadocument.Find(id);
      if (eur_ideadocument == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideadocument);
    }

    // GET: IdeaDocument/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: IdeaDocument/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "ID_AutoID,ID_IDPID,ID_Type,ID_Path,ID_State,ID_AddDate,ID_AddUser,ID_EditDate,ID_EditUser")] eur_ideadocument eur_ideadocument)
    {
      if (ModelState.IsValid)
      {
        db.eur_ideadocument.Add(eur_ideadocument);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_ideadocument);
    }

    // GET: IdeaDocument/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideadocument eur_ideadocument = db.eur_ideadocument.Find(id);
      if (eur_ideadocument == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideadocument);
    }

    // POST: IdeaDocument/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "ID_AutoID,ID_IDPID,ID_Type,ID_Path,ID_State,ID_AddDate,ID_AddUser,ID_EditDate,ID_EditUser")] eur_ideadocument eur_ideadocument)
    {
      if (ModelState.IsValid)
      {
        db.Entry(eur_ideadocument).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_ideadocument);
    }

    // GET: IdeaDocument/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideadocument eur_ideadocument = db.eur_ideadocument.Find(id);
      if (eur_ideadocument == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideadocument);
    }

    // POST: IdeaDocument/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_ideadocument eur_ideadocument = db.eur_ideadocument.Find(id);
      db.eur_ideadocument.Remove(eur_ideadocument);
      db.SaveChanges();
      return RedirectToAction("Index");
    }
    // POST: IdeaDocument/Delete/5
    [HttpPost]
    public ActionResult DeleteFile(int id)
    {
      eur_ideadocument eur_ideadocument = db.eur_ideadocument.Find(id);
      db.eur_ideadocument.Remove(eur_ideadocument);
      db.SaveChanges();
      return new JsonResult { Data = new { status = true } };
    }
    [HttpPost]
    public ActionResult CreateFile(int id, string tipo)
    {
     
      if (Request.Files.Count>0)
      {
        var Upload = Request.Files[0];
        if (Upload.ContentLength > 0)
        {
          DirectoryInfo dir = new DirectoryInfo(Server.MapPath("~/Documentos/Idea/" + id + "/" + tipo));
          if (!dir.Exists)
          {
            dir.Create();
          }
          string path = Path.Combine(dir.ToString(), Path.GetFileName(Upload.FileName));
          Upload.SaveAs(path);
          eur_ideadocument doc = new eur_ideadocument
          {
            ID_AddDate = DateTime.Now,
            ID_AddUser = User.Identity.Name,
            ID_EditDate = DateTime.Now,
            ID_EditUser = User.Identity.Name,
            ID_IDPID = id,
            ID_Path = "Idea/" + id + "/" + tipo + "/" + Upload.FileName,
            ID_State = "A",
            ID_Type = tipo
          };
          db.eur_ideadocument.Add(doc);
          db.SaveChanges();

          return new JsonResult { Data = new { status = true,Name=doc.ID_Path, id=doc.ID_AutoID } };
        }
      }
      return new JsonResult { Data = new { status = false } };

      
    }
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
