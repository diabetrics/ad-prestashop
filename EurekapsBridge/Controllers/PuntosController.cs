﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EurekapsBridge.Models;
using EurekapsBridge.Code.Security;
using EurekapsBridge.Code.Helpers;

namespace EurekapsBridge.Controllers
{
  public class PuntosController : Controller
  {
    private InngeniaEntities db = new InngeniaEntities();

    // GET: eur_ideapoints
    public ActionResult Index()
    {
      return View(db.eur_ideapoints.Where(a=> a.IUP_User == User.Identity.Name).ToList());
    }
    [PrixmaAuthorize]
    [HttpPost, Authorize]
    public ActionResult AsignarPuntos(string Tipo, string campID)
    {
      try {
        switch (Tipo)
        {
          case "ID":
            GuardarEvaluacionIdea(campID);
            break;
          case "IN":
            GuardarEvaluacionIniciativa(campID);
            break;
        }
        db.SaveChanges();
        return new JsonResult { Data = true };
      }
      catch (Exception e)
      {
        return new JsonResult { Data = false };
      }
    }

    private void GuardarEvaluacionIniciativa(string campID)
    {
      var evaluadas = db.eur_ideaprofile.Where(a => a.IDP_CampainID == campID && a.IDP_Status == "E");
      var datose = new List<eur_ideaprofile>();
      foreach (var ent in evaluadas)
      {
        eur_ideapoints existe;
        using (var db = new InngeniaEntities())
        {
          existe = db.eur_ideapoints.Where(a => a.IUP_IDPID == ent.IDP_AutoID && a.IUP_Type == "IN").FirstOrDefault();
        }
        if (existe == null)
        {
          double total = GetTotalEvaluacion(ent);
          PuntuarDuenoIniciativa(campID, ent, total);
          PuntuarCompañeros(campID, ent, total);
          PuntuarPadrinos(campID, ent);

        }
      }
    }

    private void PuntuarPadrinos(string campID, eur_ideaprofile ent)
    {
      using (var db1 = new InngeniaEntities())
      {
        var padrinos = (from e in db1.eur_ideasponsor
                        where e.IS_IDPID == ent.IDP_AutoID
                        select e).ToList();
        var puntos = 10;
        foreach (var p in padrinos)
        {
          bool existe = false;
          using (var db2 = new InngeniaEntities())
          {
            existe = db2.eur_ideapoints.Where(a => a.IUP_IDPID == ent.IDP_AutoID && p.IS_User == a.IUP_User) == null;
          }
          if (!existe)
          {
            db1.eur_ideapoints.Add(new eur_ideapoints
            {
              IUP_AddDate = DateTime.Now,
              IUP_AddUser = User.Identity.Name,
              IUP_CAMID = campID,
              IUP_EditDate = DateTime.Now,
              IUP_EditUser = User.Identity.Name,
              IUP_IDPID = ent.IDP_AutoID,
              IUP_Points = puntos,
              IUP_Type = "P",
              IUP_User = p.IS_User
            });
          }
          Utilshelper. AsignarShopPoints(puntos, p.IS_User);
        }
        db1.SaveChanges();
      }

    }

    private static void AsignarShopPoints( int puntos, string User)
    {
      using (var db = new InngeniaEntities())
      {
        var shopid = Utils.getShopId(User);
        var cr = db.ps_cart_rule.Where(a => a.quantity == 1 && a.id_customer == shopid && a.active).FirstOrDefault();
        if (cr == null)
        {
          var max = db.ps_cart_rule.Max(a => a.id_cart_rule);
          cr = new ps_cart_rule
          {
            id_cart_rule = max + 1,
            id_customer = shopid,
            date_from = DateTime.Now,
            date_to = DateTime.Now.AddYears(1),
            quantity = 1,
            quantity_per_user = 1,
            priority = 1,
            partial_use = true,
            code = "",
            minimum_amount = 0,
            minimum_amount_tax = false,
            minimum_amount_currency = 4,
            minimum_amount_shipping = false,
            country_restriction = false,
            carrier_restriction = false,
            group_restriction = false,
            cart_rule_restriction = false,
            product_restriction = false,
            shop_restriction = false,
            free_shipping = false,
            reduction_percent = 0,
            reduction_amount = puntos,
            reduction_tax = true,
            reduction_currency = 4,
            reduction_product = 0,
            gift_product = 0,
            gift_product_attribute = 0,
            highlight = true,
            active = true,
            date_add = DateTime.Now,
            date_upd = DateTime.Now,
            description = "Puntos Eurekaps"
          };
          db.ps_cart_rule.Add(cr);
        }
        else
        {
          cr.reduction_amount += puntos;
          cr.date_upd = DateTime.Now;
          cr.date_to = cr.date_to.AddYears(1);
          db.Entry(cr).State = EntityState.Modified;
        }
        db.SaveChanges();
      }
    }

    private void PuntuarCompañeros(string campID, eur_ideaprofile ent, double total)
    {
      using (var db1 = new InngeniaEntities())
      {
        var compañeros = (from e in db1.eur_ideapartner
                          where e.IP_IDPID == ent.IDP_AutoID
                          select e).ToList();
        var puntos = (int)Math.Ceiling(total);
        foreach (var com in compañeros)
        {
          bool existe = false;
          using (var db2 = new InngeniaEntities())
          {
            existe = db2.eur_ideapoints.Where(a => a.IUP_IDPID == ent.IDP_AutoID && com.IP_User == a.IUP_User) == null;
          }
          if (!existe)
          {
            db1.eur_ideapoints.Add(new eur_ideapoints
            {
              IUP_AddDate = DateTime.Now,
              IUP_AddUser = User.Identity.Name,
              IUP_CAMID = campID,
              IUP_EditDate = DateTime.Now,
              IUP_EditUser = User.Identity.Name,
              IUP_IDPID = ent.IDP_AutoID,
              IUP_Points = puntos,
              IUP_Type = "C",
              IUP_User = com.IP_User
            });
           Utilshelper.AsignarShopPoints(puntos, com.IP_User);
          }
        }
        db1.SaveChanges();
      }
    }

    private void PuntuarDuenoIniciativa(string campID, eur_ideaprofile ent, double total)
    {
      using (var db1 = new InngeniaEntities())
      {
        bool existe = false;
        var puntos = (int)Math.Ceiling(total);
        using (var db2 = new InngeniaEntities())
        {
          existe = db2.eur_ideapoints.Where(a => a.IUP_IDPID == ent.IDP_AutoID && ent.IDP_AddUser == a.IUP_User) == null;
        }
        if (!existe)
        {
          db1.eur_ideapoints.Add(new eur_ideapoints
          {
            IUP_AddDate = DateTime.Now,
            IUP_AddUser = User.Identity.Name,
            IUP_CAMID = campID,
            IUP_EditDate = DateTime.Now,
            IUP_EditUser = User.Identity.Name,
            IUP_IDPID = ent.IDP_AutoID,
            IUP_Points = puntos,
            IUP_Type = "IN",
            IUP_User = ent.IDP_AddUser
          });
          Utilshelper.AsignarShopPoints(puntos, ent.IDP_AddUser);
        }
        db1.SaveChanges();
      }
    }

    private static double GetTotalEvaluacion(eur_ideaprofile ent)
    {
      double gl, c, r;
      using (var db1 = new InngeniaEntities())
      {
        gl = (from e in db1.eur_evalgeneral
              where e.CEG_IDPID == ent.IDP_AutoID
              group e by e.CEG_IDPID into g
              select g.Average(a => a.CEG_Value)).FirstOrDefault();
      }
      using (var db1 = new InngeniaEntities())
      {
        c = (from e in db1.eur_evalcriteria
             where e.EC_IDPID == ent.IDP_AutoID
             group e by e.EC_IDPID into g
             select g.Average(a => a.EC_Value)).FirstOrDefault();
      }
      using (var db1 = new InngeniaEntities())
      {
        r = (from e in db1.eur_evalreq
             where e.CER_IDPID == ent.IDP_AutoID
             group e by e.CER_IDPID into g
             select g.Average(a => a.CER_Value)).FirstOrDefault();
      }
      ent.IDP_TotalEval = (gl + c + r) / 3;
      double total = ((ent.IDP_TotalEval * 35 / 10) + 15);
      return total;
    }

    private void GuardarEvaluacionIdea(string campID)
    {
      var ideas = (from a in db.eur_ideaprofile
                   where a.IDP_CampainID == campID
                   group a by a.IDP_AddUser into gr
                   select gr.Key).ToList();

      var puntos = (from a in db.eur_ideapoints
                    where a.IUP_CAMID == campID && a.IUP_Type == "ID"
                    group a by a.IUP_User into gr
                    select gr.Key).ToList();


      foreach (var ent in ideas)
      {
        var has = puntos.Where(a => a.Contains(ent)).FirstOrDefault();
        if (has == null)
        {
          eur_ideapoints idp = new eur_ideapoints
          {
            IUP_AddDate = DateTime.Now,
            IUP_AddUser = User.Identity.Name,
            IUP_CAMID = campID,
            IUP_EditDate = DateTime.Now,
            IUP_EditUser = User.Identity.Name,
            IUP_Points = 10,
            IUP_Type = "ID",
            IUP_User = ent
          };
          db.eur_ideapoints.Add(idp);
          Utilshelper. AsignarShopPoints(10, ent);
        }
      }
      db.SaveChanges();
    }

    [PrixmaAuthorize]
    // GET: eur_ideapoints/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideapoints eur_ideapoints = db.eur_ideapoints.Find(id);
      if (eur_ideapoints == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideapoints);
    }

    [PrixmaAuthorize]
    // GET: eur_ideapoints/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: eur_ideapoints/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [PrixmaAuthorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "IUP_AutoID,IUP_IDPID,IUP_User,IUP_Points,IUP_Used,IUP_AddDate,IUP_AddUser,IUP_EditDate,IUP_EditUser")] eur_ideapoints eur_ideapoints)
    {
      if (ModelState.IsValid)
      {
        db.eur_ideapoints.Add(eur_ideapoints);
        db.SaveChanges();
        return RedirectToAction("Index");
      }

      return View(eur_ideapoints);
    }

    [PrixmaAuthorize]
    // GET: eur_ideapoints/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideapoints eur_ideapoints = db.eur_ideapoints.Find(id);
      if (eur_ideapoints == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideapoints);
    }

    // POST: eur_ideapoints/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [PrixmaAuthorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "IUP_AutoID,IUP_IDPID,IUP_User,IUP_Points,IUP_Used,IUP_AddDate,IUP_AddUser,IUP_EditDate,IUP_EditUser")] eur_ideapoints eur_ideapoints)
    {
      if (ModelState.IsValid)
      {
        db.Entry(eur_ideapoints).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(eur_ideapoints);
    }

    [PrixmaAuthorize]
    // GET: eur_ideapoints/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      eur_ideapoints eur_ideapoints = db.eur_ideapoints.Find(id);
      if (eur_ideapoints == null)
      {
        return HttpNotFound();
      }
      return View(eur_ideapoints);
    }

    // POST: eur_ideapoints/Delete/5
    [PrixmaAuthorize]
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      eur_ideapoints eur_ideapoints = db.eur_ideapoints.Find(id);
      db.eur_ideapoints.Remove(eur_ideapoints);
      db.SaveChanges();
      return RedirectToAction("Index");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
  struct dato
  {
    public string usuario { get; set; }
    public string idea { get; set; }
   public double evaluacion { get; set; }
  }
}
