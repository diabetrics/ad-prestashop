﻿using EurekapsBridge.Code.Security;
using EurekapsBridge.Models;
using Fluentx.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using EurekapsBridge.Code.Helpers;

namespace EurekapsBridge.Controllers
{
  [Authorize]
  public class HomeController : BaseController
  {
    public ActionResult Index()
    {
      return View();
    }

    [AllowAnonymous]
    public ActionResult Login(string ReturnUrl = null)
    {
      ViewBag.ReturnUrl = ReturnUrl;
      return View();
    }
    [HttpPost, AllowAnonymous]
    public ActionResult Login(LoginViewModel model, string ReturnUrl)
    {
      if (ModelState.IsValid)
      {
        eur_account acc;
        bool IsvalidUser = Utils.IsValidUser(model.UserName, model.Password, out acc);
        //var IsvalidUser = true;
        
        if (IsvalidUser)
        {
          using (var db = new InngeniaEntities())
          {
            acc = db.eur_account.Where(a => a.ACP_User == model.UserName).FirstOrDefault();
            acc.ACP_LastLoginDate = DateTime.Now;
            db.Entry(acc).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
          }

          long idShop = 0;
          if (!Utils.IsShopUser(acc.ACP_Mail, out idShop))
            Utils.CreateShopUser(acc);
          Utils.AsignarGrupoShop(idShop);
          acc.ShopId = idShop;
          string userData = JsonConvert.SerializeObject(acc);
          FormsAuthenticationTicket auth = new FormsAuthenticationTicket(
              1, model.UserName, DateTime.Now, DateTime.Now.AddMinutes(15), false, userData
              );

          string enct = FormsAuthentication.Encrypt(auth);

          HttpCookie fa = new HttpCookie(FormsAuthentication.FormsCookieName, enct);

          Response.Cookies.Add(fa);
          if (!string.IsNullOrEmpty(ReturnUrl))
            return Redirect(ReturnUrl);
          return RedirectToAction("Index");

        }
        else
        {
          ModelState.AddModelError("", "El nombre de usuario o contraseña son incorrectos.");
          ViewBag.Error = "Datos Errados";
        }


      }
      return View(model);
    }
    public ActionResult LogOut()
    {
      FormsAuthentication.SignOut();

      return RedirectToAction("Login");
    }

    [Authorize]
    public ActionResult Dash()
    {
      return View();
    }

    [Authorize]
    public ActionResult GotoShop()
    {
      var rshop = ConfigurationManager.AppSettings["rshop"];
      long idShop;
      if (!Utils.IsShopUser(User.Account.ACP_Mail, out idShop))
        Utils.CreateShopUser(User.Account);
      Utils.AsignarGrupoShop(idShop);
      Dictionary<string, object> postData = new Dictionary<string, object>();
      postData.Add("email", User.Account.ACP_Mail);
      postData.Add("site", "index.php");
      return this.RedirectAndPost(rshop, postData);
    }
    [Authorize]
    public ActionResult GotoPoints()
    {
      var rshop = ConfigurationManager.AppSettings["rshop"];
      long idShop;
      if (!Utils.IsShopUser(User.Account.ACP_Mail,out idShop))
        Utils.CreateShopUser(User.Account);
      Utils.AsignarGrupoShop(idShop);
      Dictionary<string, object> postData = new Dictionary<string, object>();
      postData.Add("email", User.Account.ACP_Mail);
      postData.Add("site", "index.php?controller=discount");
      return this.RedirectAndPost(rshop, postData);
    }
    [Authorize]
    public ActionResult GotoShopPoints()
    {
      var rshop = ConfigurationManager.AppSettings["rshop"];
      long idShop;
      if (!Utils.IsShopUser(User.Account.ACP_Mail,out idShop))
        Utils.CreateShopUser(User.Account);
      Utils.AsignarGrupoShop(idShop);
      Dictionary<string, object> postData = new Dictionary<string, object>();
      postData.Add("email", User.Account.ACP_Mail);
      return this.RedirectAndPost(rshop, postData);
    }

    
    [Authorize]
    public ActionResult GotoCMS()
    {
      var rshop = ConfigurationManager.AppSettings["rblog"];

      if (!Utils.IsCMSUser(User.Identity.Name))
        Utils.CreateShopUser(User.Account);
      Dictionary<string, object> postData = new Dictionary<string, object>();
      postData.Add("username", User.Account.ACP_User);
      return this.RedirectAndPost(rshop, postData);
    }
    [AllowAnonymous]
    public ActionResult Error()
    {
      return View();
    }
    [AllowAnonymous]
    public ActionResult ErrorF()
    {
      return View();
    }

  }
}