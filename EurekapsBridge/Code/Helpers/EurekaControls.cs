﻿using EurekapsBridge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

public static class EurekaControls
  {
    public static MvcHtmlString StateFor<TModel, TProperty>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TProperty>> expr, Object htmlAttributes = null)
    {
      var data = ModelMetadata.FromLambdaExpression(expr, htmlhelper.ViewData);
      List<SelectListItem> li = new List<SelectListItem>();
    li.Add(new SelectListItem() { Text = "Activo", Value = "A", Selected = data.Model != null ? data.Model.ToString() == "A" : false });
    li.Add(new SelectListItem() { Text = "Inactivo", Value = "I", Selected = data.Model != null ? data.Model.ToString() == "I" : false });
    return System.Web.Mvc.Html.SelectExtensions.DropDownList(htmlhelper, data.PropertyName, li, htmlAttributes);
    }
  public static MvcHtmlString FieldTypeFor<TModel, TProperty>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TProperty>> expr, Object htmlAttributes = null)
  {
    var data = ModelMetadata.FromLambdaExpression(expr, htmlhelper.ViewData);
    List<SelectListItem> li = new List<SelectListItem>();
    li.Add(new SelectListItem() { Text = "Texto", Value = "T", Selected = data.Model != null ? data.Model.ToString() == "T" : false });
    li.Add(new SelectListItem() { Text = "Escala", Value = "E", Selected = data.Model != null ? data.Model.ToString() == "E" : false });
    li.Add(new SelectListItem() { Text = "Numero", Value = "N", Selected = data.Model != null ? data.Model.ToString() == "N" : false });

    return System.Web.Mvc.Html.SelectExtensions.DropDownList(htmlhelper, data.PropertyName, li, htmlAttributes);
  }
  public static MvcHtmlString AreasFor<TModel, TProperty>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TProperty>> expr, object htmlAttributes = null)
  {
    ModelMetadata data = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expr, htmlhelper.ViewData);
    List<SelectListItem> list1 = new List<SelectListItem>();
    list1.Add(new SelectListItem() { Text = "", Value = "" });
    using (InngeniaEntities db = new InngeniaEntities())
    {

      var datos = db.eur_areaprofile.Where(a => a.AP_State == "A");
      foreach (var area in datos)
      {
        list1.Add(new SelectListItem { Text = area.AP_Name, Value = area.AP_AutoID.ToString(), Selected = data.Model != null ? data.Model.ToString() == area.AP_AutoID.ToString() : false });
      }
      return SelectExtensions.DropDownList((HtmlHelper)htmlhelper, data.PropertyName, list1, htmlAttributes);
    }
  }

  public static MvcHtmlString EmpresasFor<TModel, TProperty>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TProperty>> expr, object htmlAttributes = null)
  {
    ModelMetadata data = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expr, htmlhelper.ViewData);
    List<SelectListItem> list1 = new List<SelectListItem>();
    list1.Add(new SelectListItem() { Text = "", Value = "" });
    using (InngeniaEntities db = new InngeniaEntities())
    {

      var datos = db.eur_enterpriseprofile.Where(a => a.EP_State == "A");
      foreach (var area in datos)
      {
        list1.Add(new SelectListItem { Text = area.EP_Name, Value = area.EP_AutoID.ToString(), Selected = data.Model != null ? data.Model.ToString() == area.EP_AutoID.ToString() : false });
      }
      return SelectExtensions.DropDownList((HtmlHelper)htmlhelper, data.PropertyName, list1, htmlAttributes);
    }
  }

}
