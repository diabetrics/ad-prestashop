﻿using EurekapsBridge.Code.Security;
using EurekapsBridge.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EurekapsBridge.Code.Helpers
{
  public static class Utilshelper
  {
    public static void AsignarShopPoints(int puntos, string User)
    {
      using (var db = new InngeniaEntities())
      {
        var shopid = Utils.getShopId(User);
        var cr = db.ps_cart_rule.Where(a => a.quantity == 1 && a.id_customer == shopid && a.active).FirstOrDefault();
        if (cr == null)
        {
          long max = 1;
          if (db.ps_cart_rule.Count() != 0)
            max = db.ps_cart_rule.Max(a => a.id_cart_rule);
          var id = max + 1;
          cr = new ps_cart_rule
          {
            id_cart_rule = id,
            id_customer = shopid,
            date_from = DateTime.Now,
            date_to = DateTime.Now.AddYears(1),
            quantity = 1,
            quantity_per_user = 1,
            priority = 1,
            partial_use = true,
            code = "",
            minimum_amount = 0,
            minimum_amount_tax = false,
            minimum_amount_currency = 4,
            minimum_amount_shipping = false,
            country_restriction = false,
            carrier_restriction = false,
            group_restriction = false,
            cart_rule_restriction = false,
            product_restriction = false,
            shop_restriction = false,
            free_shipping = false,
            reduction_percent = 0,
            reduction_amount = puntos,
            reduction_tax = true,
            reduction_currency = 4,
            reduction_product = 0,
            gift_product = 0,
            gift_product_attribute = 0,
            highlight = true,
            active = true,
            date_add = DateTime.Now,
            date_upd = DateTime.Now,
            description = "Puntos Eurekaps"
          };
          var crlg = new ps_cart_rule_lang
          {
            id_cart_rule = id,
            id_lang = 3,
            name = "Puntos Eurekaps"
          };
          db.Database.ExecuteSqlCommand(
            "INSERT INTO `ps_cart_rule`(`id_cart_rule`, `id_customer`, `date_from`, `date_to`, `description`, `quantity`, `quantity_per_user`, `priority`, `partial_use`, `code`, `minimum_amount`, `minimum_amount_tax`, `minimum_amount_currency`, `minimum_amount_shipping`, `country_restriction`, `carrier_restriction`, `group_restriction`, `cart_rule_restriction`, `product_restriction`, `shop_restriction`, `free_shipping`, `reduction_percent`, `reduction_amount`, `reduction_tax`, `reduction_currency`, `reduction_product`, `gift_product`, `gift_product_attribute`, `highlight`, `active`, `date_add`, `date_upd`) values " +
            "(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10,@p11,@p12,@p13,@p14,@p15,@p16,@p17,@p18,@p19,@p20,@p21,@p22,@p23,@p24,@p25,@p26,@p27,@p28,@p29,@p30,@p31)",
            cr.id_cart_rule,
            cr.id_customer,
            cr.date_from,
            cr.date_to,
            cr.description,
            cr.quantity,
            cr.quantity_per_user,
            cr.priority,
            cr.partial_use,
            cr.code,
            cr.minimum_amount,
            cr.minimum_amount_tax,
            cr.minimum_amount_currency,
            cr.minimum_amount_shipping,
            cr.country_restriction,
            cr.carrier_restriction,
            cr.group_restriction,
            cr.cart_rule_restriction,
            cr.product_restriction,
            cr.shop_restriction,
            cr.free_shipping,
            cr.reduction_percent,
            cr.reduction_amount,
            cr.reduction_tax,
            cr.reduction_currency,
            cr.reduction_product,
            cr.gift_product,
            cr.gift_product_attribute,
            cr.highlight,
            cr.active,
            cr.date_add,
            cr.date_upd);
          db.Database.ExecuteSqlCommand("INSERT INTO `ps_cart_rule_lang`(`id_cart_rule`, `id_lang`, `name`) VALUES (@p0,@p1,@p2)", id, 3, "Puntos Eurekaps - " + User);
        }
        else
        {
          cr.reduction_amount += puntos;
          cr.date_upd = DateTime.Now;
          cr.date_to = cr.date_to.AddYears(1);
          db.Database.ExecuteSqlCommand("UPDATE `ps_cart_rule` SET reduction_amount=@p0, date_upd=@p1,date_to=@p2 WHERE id_cart_rule =@p3", cr.reduction_amount, cr.date_upd, cr.date_to, cr.id_cart_rule);
          db.Database.ExecuteSqlCommand("UPDATE `ps_cart_rule_lang` SET `name`=@p0 WHERE `id_cart_rule`=@p1 AND `id_lang`=@p2 ", "Puntos Eurekaps - " + User, cr.id_cart_rule, 3);
        }
      }
    }
    public static bool IsMyIdea(this eur_ideaprofile idea)
    {
      return idea.IDP_AddUser == HttpContext.Current.User.Identity.Name;
    }
  }
}