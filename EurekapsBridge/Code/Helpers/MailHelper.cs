﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using EurekapsBridge.Models;
using System.IO;

namespace EurekapsBridge.Code.Helpers
{
  public static class MailHelper
  {
    public static void SendMailNoTemplate(string to, string Asunto, string Contenido)
    {


      MailAddress toAddress = new MailAddress(to);
      MailMessage message = new MailMessage();
      message.To.Add(toAddress);
      message.From = new MailAddress("eurekaps@procaps.com.co");
      message.Subject = Asunto;
      message.Body = Contenido;
      message.IsBodyHtml = true;

      SmtpClient smtp = new SmtpClient("smtp.office365.com", 587);
      smtp.EnableSsl = true;
      smtp.UseDefaultCredentials = false;
      smtp.Credentials = new System.Net.NetworkCredential("eurekaps@procaps.com.co", "Barranquilla2016");
      smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
     smtp.Send(message);

    }
    public static void SendMail(string to, string Asunto, string plantilla = "", Dictionary<string, string> Valores = null)
    {
      if (string.IsNullOrEmpty(plantilla))
        plantilla = "mail";
      string contenido = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/" + plantilla + ".html"));
      if (Valores != null)
        foreach (var key in Valores.Keys)
        {
          contenido = contenido.Replace("@@" + key, Valores[key]);
        }
      SendMailNoTemplate(to, Asunto, contenido);

    }

  }
}