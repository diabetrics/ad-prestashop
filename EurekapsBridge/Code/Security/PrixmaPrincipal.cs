﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Bukimedia.PrestaSharp.Entities;
using Bukimedia.PrestaSharp.Factories;
using System.Configuration;
using EurekapsBridge.Models;

namespace EurekapsBridge.Code.Security
{
  public class PrixmaPrincipal : IPrincipal
  {
    public eur_account Account;
    
    public IIdentity Identity
    {
      get; private set;
    }

    public bool IsInRole(string role)
    {
      switch (role)
      {
        default: return false;
      }
    }
    public PrixmaPrincipal(eur_account Acc)
    {
      Account = Acc;
      this.Identity = new GenericIdentity(Acc.ACP_User);

    }
  }
}