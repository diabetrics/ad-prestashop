﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EurekapsBridge.Code.Security
{
  [Authorize]
  public class BaseController : Controller
  {
    protected virtual new PrixmaPrincipal User
    {
      get { return HttpContext.User as PrixmaPrincipal; }
    }
    protected virtual bool IsAuthenticated
    {
      get { return HttpContext.User.Identity.IsAuthenticated; }
    }
  }
}