﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EurekapsBridge.Code.Security
{
    public abstract class BaseViewPage : WebViewPage
    {
        public virtual new PrixmaPrincipal User
        {
            get { return base.User as PrixmaPrincipal; }
        }
        public bool IsAuthenticated
        {
            get { return base.User.Identity.IsAuthenticated; }
        }
        
    }
    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new PrixmaPrincipal User
        {
            get { return base.User as PrixmaPrincipal; }
        }
        public bool IsAuthenticated
        {
            get { return base.User.Identity.IsAuthenticated; }
        }
    }
}