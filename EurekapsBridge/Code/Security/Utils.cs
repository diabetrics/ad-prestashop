﻿using Bukimedia.PrestaSharp.Factories;
using EurekapsBridge.Code.Helpers;
using EurekapsBridge.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using WordPressSharp.Models;

namespace EurekapsBridge.Code.Security
{
  public class Utils
  {
    public static bool IsValidUser(string username, string password, out eur_account account)
    {
      account = null;
      string dominio = ConfigurationManager.AppSettings["dominio"];
      using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, dominio))
      {
        // validate the credentials
        bool isValid = pc.ValidateCredentials(username, password);
        if (isValid)
        {
          var data = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, dominio + "\\" + username);

          if (!IsInngeniaUser(username))
          {
            account = new eur_account
            {
              ACP_Mail = data.EmailAddress,
              ACP_ID = data.EmployeeId,
              ACP_FirstName = string.IsNullOrEmpty(data.GivenName) ? data.DisplayName : data.GivenName,
              ACP_LastName = string.IsNullOrEmpty(data.Surname) ? data.DisplayName : data.Surname,
              ACP_User = data.Name
            };
            CreateInngeniaUser(account);
           
          }
        }
        else
        {
          using (var db = new InngeniaEntities())
          {
            string passmd5 = password.ToMD5();
            var acc = db.eur_account.Where(a => a.ACP_User == username && a.ACP_Password == passmd5).FirstOrDefault();
            isValid = acc != null;
          }
        }
        return isValid;
      }
    }
    public static bool IsShopUser(string mail, out long id)
    {
      id = 0;
      CustomerFactory factory = new CustomerFactory(General.BaseUrl, General.APIAccount, General.APIPassword);
      Dictionary<string, string> dtn = new Dictionary<string, string>();
      dtn.Add("email", mail);
      var existe = factory.GetByFilter(dtn, null, null).FirstOrDefault();
      if (existe != null)
        id = existe.id.Value;
      return existe != null;
    }
    public static long getShopId(string User)
    {
      using (var db = new InngeniaEntities())
      {
        eur_account ent = db.eur_account.Where(a => a.ACP_User == User).FirstOrDefault();
        CustomerFactory factory = new CustomerFactory(General.BaseUrl, General.APIAccount, General.APIPassword);
        Dictionary<string, string> dtn = new Dictionary<string, string>();
        dtn.Add("email", ent.ACP_Mail);
        var existe = factory.GetByFilter(dtn, null, null).FirstOrDefault();
        return existe.id.Value;
      }
    }
    public static bool IsShopAdmin(string mail)
    {
      EmployeeFactory factory = new EmployeeFactory(General.BaseUrl, General.APIAccount, General.APIPassword);
      Dictionary<string, string> dtn = new Dictionary<string, string>();
      dtn.Add("email", mail);
      var existe = factory.GetByFilter(dtn, null, null);
      return existe.Count != 0;
    }
    public static void CreateShopUser(eur_account cliente)
    {
      CustomerFactory factory = new CustomerFactory(General.BaseUrl, General.APIAccount, General.APIPassword);


      Bukimedia.PrestaSharp.Entities.customer cus = new Bukimedia.PrestaSharp.Entities.customer()
      {
        active = 1,
        date_add = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
        email = cliente.ACP_Mail,
        firstname = cliente.ACP_FirstName,
        id_shop = 1,
        id_shop_group = 1,
        id_default_group = 3,
        id_lang = 3,
        id_risk = 0,
        lastname = cliente.ACP_LastName,
        id_gender = 1,
        ape = null,
        birthday = null,
        company = null,
        date_upd = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
        deleted = 0,
        siret = null,
        passwd = "",
        last_passwd_gen = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
        newsletter = 1,
        ip_registration_newsletter = null,
        newsletter_date_add = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
        optin = 1,
        website = null,
        outstanding_allow_amount = 0,
        show_public_prices = 0,
        max_payment_days = 0,
        note = null,
        is_guest = 0,

      };
      factory.Add(cus);
      Dictionary<string, string> dtn = new Dictionary<string, string>();
      dtn.Add("email", cliente.ACP_Mail);
      cus = factory.GetByFilter(dtn, null, null).First();
      AddressFactory adfac = new AddressFactory(General.BaseUrl, General.APIAccount, General.APIPassword);


      Bukimedia.PrestaSharp.Entities.address addr = new Bukimedia.PrestaSharp.Entities.address()
      {
        address1 = "Calle 80 # 78B - 201",
        address2 = "",
        alias = "Direccion",
        city = "Barranquilla",
        company = "Procaps",
        date_add = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
        date_upd = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
        deleted = 0,
        dni = cliente.ACP_ID,
        firstname = cliente.ACP_FirstName,
        id_country = 69,
        id_customer = cus.id.Value,
        id_manufacturer = 0,
        id_state = 0,
        id_supplier = 0,
        id_warehouse = 0,
        lastname = cliente.ACP_LastName,
        phone = "",
        phone_mobile = "",
        other = "",
        postcode = "",
        vat_number = ""

      };
     
      adfac.Add(addr);

      AsignarGrupoShop(cus.id.Value);

    }

    public static void AsignarGrupoShop(long cus)
    {
      using (var db = new InngeniaEntities())
      {
        if (db.ps_customer_group.Where(a => a.id_customer == cus && a.id_group == 3).FirstOrDefault() == null)
        {
          db.ps_customer_group.Add(new ps_customer_group { id_customer = (int)cus, id_group = 3 });
          db.SaveChanges();
        }
      }
    }

    public static bool IsCMSUser(string username)
    {
      WordPressSharp.WordPressClient cliente = new WordPressSharp.WordPressClient();
      
        var filtro = new UserFilter();
        filtro.Role = "subscriber";
        filtro.Who = "";
        filtro.OrderBy = "";
        filtro.Order = "";
        var existe = cliente.GetUsers(filtro).Where(a => a.Username == username).FirstOrDefault();
        return existe != null;

      

    }
    public static bool IsCMSAdmin(string username)
    {
      WordPressSharp.WordPressClient cliente = new WordPressSharp.WordPressClient();
      
        var filtro = new UserFilter();
        filtro.Role = "administrator";
        filtro.Who = "";
        filtro.OrderBy = "";
        filtro.Order = "";
        var existe = cliente.GetUsers(filtro).Where(a => a.Username == username).FirstOrDefault();
        return existe != null;

      

    }
    public static void CreateCMSUser(eur_account cliente)
    {
      string wpcuURL = ConfigurationManager.AppSettings["WordPressCreateUserURL"];
      string wpURL = ConfigurationManager.AppSettings["WordPressBaseUrl"];
      var client = new RestSharp.RestClient(wpcuURL);
      var request = new RestSharp.RestRequest(RestSharp.Method.POST);
      request.AddParameter("username", cliente.ACP_User);
      request.AddParameter("sitio", wpURL);
      request.AddParameter("email", cliente.ACP_Mail);
      request.AddParameter("nombre", cliente.ACP_FirstName);
      request.AddParameter("apellido", cliente.ACP_LastName);
      RestSharp.IRestResponse response = client.Execute(request);
      var content = response.Content;
    }
    public static void LoginShop(string email)
    {
      string rblog = ConfigurationManager.AppSettings["rshop"];
      var client = new RestSharp.RestClient(rblog);
      var req = new RestSharp.RestRequest(RestSharp.Method.POST);
      req.AddParameter("email", email);

      RestSharp.IRestResponse response = client.Execute(req);

      var content = response.Content;
    }
    public static void LoginCMS(string username)
    {
      string rblog = ConfigurationManager.AppSettings["rblog"];
      var client = new RestSharp.RestClient(rblog);
      var req = new RestSharp.RestRequest(RestSharp.Method.POST);
      req.AddParameter("username", username);

      RestSharp.IRestResponse response = client.Execute(req);

      var content = response.Content;
    }

    public static bool IsInngeniaUser(string username)
    {
      using (InngeniaEntities db = new InngeniaEntities())
      {
        var accdb = db.eur_account.Where(a => a.ACP_User == username).FirstOrDefault();
        return accdb != null;
      }
    }

    public static void CreateInngeniaUser(eur_account acc)
    {
      using (InngeniaEntities db = new InngeniaEntities())
      {
        acc.ACP_AddDate = DateTime.Now;
        acc.ACP_LastLoginDate = DateTime.Now;
        acc.ACP_IsAdmin = "N";
        db.eur_account.Add(acc);
       
          db.SaveChanges();
          Dictionary<string, string> Datos = new Dictionary<string, string>();
          Datos.Add("Nombre", acc.ACP_FirstName);
          /*MailHelper.SendMail(acc.ACP_Mail,
             "Bienvenido a Eurekaps","Bienvenida",Datos);*/

      
      }

    }

    public static void SaveLastLogin(string username)
    {
      using(var db = new InngeniaEntities())
      {
        var acc = db.eur_account.Where(a => a.ACP_User == username).FirstOrDefault();
        acc.ACP_LastLoginDate = DateTime.Now;
        db.Entry(acc).State = System.Data.Entity.EntityState.Modified;
        db.SaveChanges();
      }
    }
    public static void CreateInngeniaUser(string Username, out eur_account account)
    {
      string dominio = ConfigurationManager.AppSettings["dominio"];
      using (PrincipalContext context = new PrincipalContext(ContextType.Domain, dominio))
      {
        UserPrincipal byIdentity = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, dominio + "\\" + Username);
        account = new eur_account();
        account.ACP_Mail = byIdentity.EmailAddress;
        account.ACP_ID = byIdentity.EmployeeId;

        account.ACP_FirstName = byIdentity.GivenName;
        account.ACP_LastName = byIdentity.Surname;
        account.ACP_User = Username;
        if (Utils.IsInngeniaUser(Username))
          return;
        Utils.CreateInngeniaUser(account);
      }
    }

    public static bool IsDomainUser(string username)
    {
      string name = ConfigurationManager.AppSettings["dominio"];
      using (PrincipalContext context = new PrincipalContext(ContextType.Domain, name))
        return UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, name + "\\" + username) != null;
    }
  }
}