﻿using EurekapsBridge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace EurekapsBridge.Code.Security
{
  public class PrixmaAuthorizeAttribute : AuthorizeAttribute
  {
    protected virtual PrixmaPrincipal CurrentUser
    {
      get { return HttpContext.Current.User as PrixmaPrincipal; }
    }
    public override void OnAuthorization(AuthorizationContext filterContext)
    {
      if (filterContext.HttpContext.Request.IsAuthenticated)
      {
        PrixmaPrincipal mp = new PrixmaPrincipal(CurrentUser.Account);
        using (var db = new InngeniaEntities())
        {
          var usr = db.eur_account.Where(a => a.ACP_User == mp.Account.ACP_User).FirstOrDefault();
          if (usr == null)
          {
            FormsAuthentication.SignOut();
            throw new Exception("Alguien intento ingresar donde no debia, algo paso con su usuario");
          }
          if (usr.ACP_IsAdmin != "S")
            throw new Exception("Alguien intento ingresar donde no debia, queria entrar a la parte de administrador sin permisos");
        }
      }
    }
  }
}