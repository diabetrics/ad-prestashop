﻿using EurekapsBridge.Code.Security;
using EurekapsBridge.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace EurekapsBridge
{
  public class MvcApplication : System.Web.HttpApplication
  {
    public object Accoun { get; private set; }

    protected void Application_Start()
    {

      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }
    protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
    {
      HttpCookie auth = Request.Cookies[FormsAuthentication.FormsCookieName];
      if (auth != null)
      {
        FormsAuthenticationTicket authti = FormsAuthentication.Decrypt(auth.Value);
        eur_account model = JsonConvert.DeserializeObject<eur_account>(authti.UserData);
        PrixmaPrincipal newuser = new PrixmaPrincipal(model);
        newuser.Account = model;
        HttpContext.Current.User = newuser;
      }
    }
    protected void Application_Error(object sender, EventArgs e)
    {
      Exception ex = Server.GetLastError();
      Server.ClearError();
      using (var db = new InngeniaEntities())
      {
        var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;
        string controller = "", action = "", user = "", innermessage = "", innerstack = "";
        if (routeValues.ContainsKey("controller"))
          controller = (string)routeValues["controller"];
        if (routeValues.ContainsKey("controller"))
          controller = (string)routeValues["controller"];
        if (routeValues.ContainsKey("action"))
          action = (string)routeValues["action"];
        if (User.Identity.IsAuthenticated)
          user = User.Identity.Name;
        if (ex.InnerException != null)
        {
          innermessage = ex.InnerException.Message;
          innerstack = ex.InnerException.StackTrace;
        }
        db.eur_exception.Add(new eur_exception
        {
          EXP_Action = action,
          EXP_AddDate = DateTime.Now,
          EXP_AddUser = user,
          EXP_Controller = controller,
          EXP_InnerMessage = innermessage,
          EXP_InnerStack = innerstack,
          EXP_Message = ex.Message,
          EXP_ModifyDate = DateTime.Now,
          EXP_ModifyUser = user,
          EXP_Stack = ex.StackTrace
        });
        db.SaveChanges();
      }
      if (ex.Message == "La campaña ya habia cerrado")
        Response.Redirect("/Home/ErrorF");
      else
        Response.Redirect("/Home/Error");
    }
  }
}
