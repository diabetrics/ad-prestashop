﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EurekapsBridge.Models
{

  public class AreaProfileMetaData
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int AP_AutoID;
    [Display(Name = "Nombre"), Required]
    public string AP_Name;
    [Display(Name = "Descripción")]
    public string AP_Description;
    [Display(Name = "Estado")]
    public string AP_State;
    public System.DateTime AP_AddDate;
    public string AP_AddUser;
    public System.DateTime AP_EditDate;
    public string AP_EditUser;
  }

  public partial class eur_ideapoints
  {
    [NotMapped]
    public string CampañaNombre
    {
      get
      {
        if (string.IsNullOrEmpty(IUP_CAMID))
          return "";
        using (var db = new InngeniaEntities())
        {
          return db.eur_campainprofile.Where(a => a.CAM_CampainID == IUP_CAMID).First().CAM_Name;
        }

      }
    }
  }
  [MetadataType(typeof(AreaProfileMetaData))]
  public partial class eur_areaprofile
  {

  }

  public class CampainCriteriaMetadata
  {
    public int CMC_AutoID;
    public string CMC_CampainID;
    public string CMC_Name;
    public string CMC_Description;
    public string CMC_Type;
    public string CMC_State;
    public System.DateTime CMC_AddDate;
    public string CMC_AddUser;
    public System.DateTime CMC_EditDate;
    public string CMC_EditUser;
  }
  public class CampainGeneralMetaData
  {
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int CMG_AutoID;
    [Display(Name = "Codigo")]
    public string CMG_CampainID;
    [Display(Name = "Nombre")]
    public string CMG_Name;
    [Display(Name = "Descripción")]
    public string CMG_Description;
    [Display(Name = "Tipo")]
    public string CMG_Type;
    [Display(Name = "Estado")]
    public string CMG_State;
    public System.DateTime CMG_AddDate;
    public string CMG_AddUser;
    public System.DateTime CMG_EditDate;
    public string CMG_EditUser;
  }
  [MetadataType(typeof(CampainGeneralMetaData))]
  public partial class eur_campaingeneral
  {

  }
  public class CampainProfileMetaData
  {
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int CAM_AutoID;
    [Display(Name = "Codigo"), Required]
    public string CAM_CampainID;
    [Display(Name = "Nombre"), Required]
    public string CAM_Name;
    [Display(Name = "Descripción"), Required]
    public string CAM_Description;
    [Display(Name = "Estado"), Required]
    public string CAM_State;
    [Display(Name = "Fecha Cierre Ideas"), Required]
    public System.DateTime CAM_FechaIdea { get; set; }
    [Display(Name = "Fecha Cierre Iniciativas"), Required]
    public System.DateTime CAM_FechaIniciativa { get; set; }

    public System.DateTime CAM_AddDate;
    public string CAM_AddUser;
    public System.DateTime CAM_EditDate;
    public string CAM_EditUser;
  }
  [MetadataType(typeof(CampainProfileMetaData))]
  public partial class eur_campainprofile
  {

  }
  public class CampainREQMetaData
  {
    public int CMR_AutoID;
    public int CMR_CampainID;
    public string CMR_Name;
    public string CMR_Description;
    public string CMR_Type;
    public string CMR_State;
    public System.DateTime CMR_AddDate;
    public string CMR_AddUser;
    public System.DateTime CMR_EditDate;
    public string CMR_EditUser;
  }
  internal sealed class EnterpriseProfileMetaData
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int EP_AutoID;
    [Display(Name = "Nombre Corto")]
    public string EP_Name;
    [Display(Name = "Nombre")]
    public string EP_Description;
    [Display(Name = "Estado")]
    public string EP_State;
    public System.DateTime EP_AddDate;
    public string EP_AddUser;
    public System.DateTime EP_EditDate;
    public string EP_EditUser;
  }
  [MetadataType(typeof(EnterpriseProfileMetaData))]
  public partial class eur_enterpriseprofile
  {

  }
  public class EvalCriteriaMetaData
  {
    public int EC_AutoID;
    public int EC_CampainID;
    public int EC_IDPID;
    public string EC_Value;
    public System.DateTime EC_AddDate;
    public string EC_AddUser;
    public System.DateTime EC_EditDate;
    public string EC_EditUser;
  }
  public class EvalGeneralMetaData
  {
    public int CEG_AutoID;
    public int CEG_CampainID;
    public int CEG_IDPID;
    public string CEG_Value;
    public System.DateTime CEG_AddDate;
    public string CEG_AddUser;
    public System.DateTime CEG_EditDate;
    public string CEG_EditUser;
  }
  public class EvalREQMetaData
  {
    public int CER_AutoID;
    public int CER_CampainID;
    public int CER_IDPID;
    public string CER_Value;
    public System.DateTime CER_AddDate;
    public string CER_AddUser;
    public System.DateTime CER_EditDate;
    public string CER_EditUser;
  }
  public class FacetProfileMetaData
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int FP_AutoID;
    [Display(Name = "Nombre")]
    public string FP_Name;
    [Display(Name = "Descripción")]
    public string FP_Description;
    [Display(Name = "Estado")]
    public string FP_State;
    [HiddenInput]
    public System.DateTime FP_AddDate
    {
      get; set;
    }
    [HiddenInput]
    public string FP_AddUser
    {
      get; set;
    }
    [HiddenInput]
    public System.DateTime FP_EditDate
    {
      get; set;
    }
    [HiddenInput]
    public string FP_EditUser
    {
      get; set;
    }
  }
  [MetadataType(typeof(FacetProfileMetaData))]
  public partial class eur_facetprofile
  {

  }
  public class IdeaDocumentMetaData
  {
    public int ID_AutoID;
    public int ID_IDPID;
    public string ID_Type;
    public string ID_Path;
    public string ID_State;
    public System.DateTime ID_AddDate;
    public string ID_AddUser;
    public System.DateTime ID_EditDate;
    public string ID_EditUser;
  }
  public class IdeaFacetMetaData
  {
    public int IF_AutoID;
    public int IF_IDPID;
    public string IF_FPID;
    public string IF_State;
    public System.DateTime IF_AddDate;
    public string IF_AddUser;
    public System.DateTime IF_EditDate;
    public string IF_EditUser;
  }
  public class IdeaPartnerMetaData
  {
    public int IP_AutoID;
    public int IP_IDPID;
    public string IP_User;
    public string IP_State;
    public System.DateTime IP_AddDate;
    public string IP_AddUser;
    public System.DateTime IP_EditDate;
    public string IP_EditUser;
  }
  public class IdeaPointsMetaData
  {
    public int IUP_AutoID;
    public int IUP_IDPID;
    public string IUP_User;
    public int IUP_Points;
    public string IUP_Used;
    public System.DateTime IUP_AddDate;
    public string IUP_AddUser;
    public System.DateTime IUP_EditDate;
    public string IUP_EditUser;
  }
  public class IdeaStateMetadata
  {
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int ISP_AutoID { get; set; }
    [Display(Name = "Nombre")]
    public string ISP_Name { get; set; }
    [Display(Name = "Descripción")]
    public string ISP_Description { get; set; }
    [Display(Name = "Estado")]
    public string ISP_State { get; set; }
    [HiddenInput]
    public System.DateTime ISP_AddDate { get; set; }
    [HiddenInput]
    public string ISP_AddUser { get; set; }
    [HiddenInput]
    public System.DateTime ISP_EditDate { get; set; }
    [HiddenInput]
    public string ISP_EditUser { get; set; }
  }
  [MetadataType(typeof(IdeaStateMetadata))]
  public partial class eur_ideastateprofile
  {
    public int TotalEvaluacion { get; set; }
  }
  public partial class IdeaProfileMetaData
  {
    [Key]
    public int IDP_AutoID { get; set; }
    [Display(Name = "Nombre de la idea"), Required]
    public string IDP_Name { get; set; }
    [Display(Name = "Descripción"), Required]
    public string IDP_Description { get; set; }
    [Display(Name = "Campaña")]
    public string IDP_CampainID { get; set; }
    [Required, Display(Name = "Área o proceso de implementación")]
    public int IDP_APID { get; set; }
    [Display(Name = "Comentarios adicionales")]
    public string IDP_Comment { get; set; }
    [Display(Name = "Creación")]
    public System.DateTime IDP_AddDate { get; set; }
    public string IDP_AddUser { get; set; }
    [Display(Name = "Última Modificación")]
    public System.DateTime IDP_ModifyDate { get; set; }
    public string IDP_ModifyUser { get; set; }
    [Display(Name = "Empresa")]
    public int IDP_EPID { get; set; }
    [Display(Name = "Situación antes de aplicar la idea")]
    public string IDP_PreIdea { get; set; }
    [Display(Name = "Situación despues de aplicar la idea")]
    public string IDP_PostIdea { get; set; }
    [Display(Name = "Alcance")]
    public string IDP_Scope { get; set; }
    [Display(Name = "Cronograma")]
    public string IDP_Schedule { get; set; }
    [Display(Name = "Descripción de la inversión")]
    public string IDP_DescInvest { get; set; }
    [Display(Name = "Inversión")]
    public Nullable<double> IDP_Investment { get; set; }
    [Display(Name = "Inversión en especie")]
    public Nullable<double> IDP_InKind { get; set; }
    [Display(Name = "Validación")]
    public string IDP_Validation { get; set; }
    [Display(Name = "Beneficios")]
    public string IDP_Benefits { get; set; }
    public string IDP_BenefitsIndicators { get; set; }
    public int IDP_ISPID { get; set; }
    public string IDP_Feedback { get; set; }

    public string IDP_Status { get; set; }


  }
  [MetadataType(typeof(IdeaProfileMetaData))]
  public partial class eur_ideaprofile
  {
    [Display(Name = "Evaluación"), NotMapped]
    public double IDP_TotalEval { get; set; }
    [Display(Name = "Nombre de la Empresa")]
    public string IDP_EnterpriseName
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if ((IDP_EPID) != 0)
          {
            var emp = db.eur_enterpriseprofile.Where(a => a.EP_AutoID == IDP_EPID).First();
            if (emp != null)
              return emp.EP_Name;
          }
        }
        return "";
      }
    }
    [Display(Name = "Nombre de la Campaña")]
    public string IDP_CampainName
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (!string.IsNullOrEmpty(IDP_CampainID))
          {
            var idea = db.eur_campainprofile.Where(a => a.CAM_CampainID == IDP_CampainID).First();
            if (idea != null)
              return idea.CAM_Name;
          }
        }
        return "";
      }
    }
    public DateTime IDP_CierrerIdea
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (!string.IsNullOrEmpty(IDP_CampainID))
          {
            var idea = db.eur_campainprofile.Where(a => a.CAM_CampainID == IDP_CampainID).First();
            if (idea != null)
              return (idea.CAM_FechaIdea);
          }
        }
        return DateTime.Now;
      }
    }
    public DateTime IDP_CierrerIniciativa
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (!string.IsNullOrEmpty(IDP_CampainID))
          {
            var idea = db.eur_campainprofile.Where(a => a.CAM_CampainID == IDP_CampainID).First();
            if (idea != null)
              return (idea.CAM_FechaIniciativa);
          }
        }
        return DateTime.Now;
      }
    }
    public double IDP_CampainCierre
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (!string.IsNullOrEmpty(IDP_CampainID))
          {
            var idea = db.eur_campainprofile.Where(a => a.CAM_CampainID == IDP_CampainID).First();
            if (idea != null)
              return (idea.CAM_FechaIniciativa - DateTime.Now).Days;
          }
        }
        return 0;
      }
    }

    public string IDP_AreaName
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (IDP_APID != 0)
          {
            var idea = db.eur_areaprofile.Where(a => a.AP_AutoID == IDP_APID).First();
            if (idea != null)
              return idea.AP_Name;
          }
        }
        return "";
      }
    }

    public string IDP_UserFullName
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (!string.IsNullOrEmpty(IDP_AddUser))
          {
            var user = db.eur_account.Where(a => a.ACP_User == IDP_AddUser).First();
            if (user != null)
              return user.ACP_FirstName + " " + user.ACP_LastName;
          }
        }
        return "";
      }
    }

  }
  public partial class eur_ideapartner
  {
    public string IP_FullName
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (!string.IsNullOrEmpty(IP_User))
          {
            var user = db.eur_account.Where(a => a.ACP_User == IP_User).First();
            if (user != null)
              return user.ACP_FirstName + " " + user.ACP_LastName;
          }
        }
        return "";
      }
    }
  }
  public partial class eur_ideasponsor
  {
    public string IS_FullName
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (!string.IsNullOrEmpty(IS_User))
          {
            var user = db.eur_account.Where(a => a.ACP_User == IS_User).First();
            if (user != null)
              return user.ACP_FirstName + " " + user.ACP_LastName;
          }
        }
        return "";
      }
    }
  }
  public class CriteriosViewModel
  {
    public string Type
    {
      get; set;
    }
    public int id
    {
      get; set;
    }
    public string Desc
    {
      get; set;
    }
    public string Name
    {
      get; set;
    }
    public string Value
    {
      get; set;
    }
  }
  [MetadataType(typeof(ProyectoMetadata))]
  public partial class eur_projectprofile
  {
    [Display(Name = "Nombre de la Idea")]
    public string PP_NombreIdea
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (PP_IDPID != 0)
          {
            var ent = db.eur_ideaprofile.Find(PP_IDPID);
            if (ent != null)
              return ent.IDP_Name;
          }
          return "";
        }
      }
    }
  }
  public partial class ProyectoMetadata
  {
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int PP_AutoID { get; set; }
    [Required]
    public int PP_IDPID { get; set; }
    [DisplayName("EDT")]
    public string PP_EDT { get; set; }
    [DataType(DataType.Date), DisplayName("Fecha Inicial")]
    public Nullable<System.DateTime> PP_InitDate { get; set; }
    [DataType(DataType.Date), DisplayName("Fecha Finalización")]
    public Nullable<System.DateTime> PP_FinishDate { get; set; }
    [DataType(DataType.Currency), DisplayName("Inversión")]
    public Nullable<decimal> PP_Investment { get; set; }
    [DataType(DataType.Currency), DisplayName("Gastos")]
    public Nullable<decimal> PP_Expenses { get; set; }
    [DisplayName("Evaluacón Financiera")]
    public string PP_FinalcialEval { get; set; }
    [Required(ErrorMessage = "Se necesita registar un lider"), DisplayName("Lider")]
    public string PP_Leader { get; set; }
    public System.DateTime PP_AddDate { get; set; }
    public string PP_AddUser { get; set; }
    public System.DateTime PP_ModifyDate { get; set; }
    public string PP_ModifyUser { get; set; }
  }
  public partial class AccountMetadata
  {
    public int ACP_AutoID { get; set; }
    [Required(ErrorMessage = "Nombre de usuario es requerido"), DisplayName("Usuario")]
    public string ACP_User { get; set; }
    [DisplayName("Contraseña")]
    public string ACP_Password { get; set; }
    [Required(ErrorMessage = "Correo Electronico es requerido"), DisplayName("Correo Electronico")]
    public string ACP_Mail { get; set; }
    public string ACP_ID { get; set; }
    [Required(ErrorMessage = "Nombre es requerido"), DisplayName("Nombres")]
    public string ACP_FirstName { get; set; }
    [Required(ErrorMessage = "Apellido es requerido"), DisplayName("Apellidos")]
    public string ACP_LastName { get; set; }
    public System.DateTime ACP_AddDate { get; set; }
    public Nullable<System.DateTime> ACP_LastLoginDate { get; set; }
    public string ACP_IsAdmin { get; set; }
  }
  [MetadataType(typeof(AccountMetadata))]
  public partial class eur_account
  {

  }
  public class ChangepasswordViewModel
  {
    public int ACP_AutoID { get; set; }
    [Required(ErrorMessage = "Nueva contraseña es requerida.")]
    [DataType(DataType.Password)]
    [DisplayName("Contraseña Nueva")]
    public string NewPassword { get; set; }

    [Required]
    [DataType(DataType.Password)]
    [System.ComponentModel.DataAnnotations.Compare("NewPassword")]
    [DisplayName("Confirmar Contraseña")]
    public string ConfirmPassword { get; set; }
  }
}
