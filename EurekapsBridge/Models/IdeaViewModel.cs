﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EurekapsBridge.Models
{
  public class IdeaViewModel
  {
    [Key]
    public int IDP_AutoID { get; set; }
    [Display(Name = "Nombre de la idea"), Required(ErrorMessage ="Debes registrar el nombre de tu idea.")]
    public string IDP_Name { get; set; }
    [Display(Name = "Descripción"), Required(ErrorMessage ="Es necesario que escribas la descripción de la idea")]
    public string IDP_Description { get; set; }
    public string IDP_CampainID { get; set; }
    [Display(Name = "Nombre de la Campaña")]
    public string IDP_CampainName
    {
      get
      {
        using (var db = new InngeniaEntities())
        {
          if (!string.IsNullOrEmpty(IDP_CampainID))
          {
            var idea = db.eur_campainprofile.Where(a => a.CAM_CampainID == IDP_CampainID).First();
            if (idea != null)
              return idea.CAM_Name;
          }
        }
        return "";
      }
    }
    [Required(ErrorMessage ="El área es un campo obligatorio"), Display(Name = "Área o proceso de implementación")]
    public int IDP_APID { get; set; }
    [Display(Name = "Comentario de la idea"),Required(ErrorMessage ="Escribe un comentario sobre la idea.")]
    public string IDP_Comment { get; set; }
    public System.DateTime IDP_AddDate { get; set; }
    public string IDP_AddUser { get; set; }
    public System.DateTime IDP_ModifyDate { get; set; }
    public string IDP_ModifyUser { get; set; }
    [DataType(DataType.Upload)]
    public HttpPostedFileBase Upload { get; set; }
    [Display(Name = "Empresa"), Required(ErrorMessage ="Debes elegir una empresa para aplicar tu idea.")]
    public int IDP_EPID { get; set; }
  }
}