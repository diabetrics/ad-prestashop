﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EurekapsBridge.Models
{
  public class CampainViewModel
  {
    [Display(Name = "Codigo"), Required]
    public string CAM_CampainID{get;set;}
    [Display(Name = "Nombre"), Required]

    public string CAM_Name{get;set;}
    [Display(Name = "Descripción"), Required]
    public string CAM_Description{get;set;}
    [Display(Name = "Estado"), Required]
    public string CAM_State{get;set;}

    [Display(Name = "Cierre de Ideas"), Required]
    public DateTime CAM_FechaIdeas { get; set; }

    [Display(Name = "Cierre de Iniciativas"), Required]
    public DateTime CAM_GFechaIniciativas { get; set; }

    public ICollection<AditionalViewModel> Requirimientos { get; set; }
    public ICollection<AditionalViewModel> Criterios { get; set; }
  }
  public partial class AditionalViewModel
  {
    public string Name { get; set; }
    public string Description { get; set; }
    public string Type { get; set; }
    public string State { get; set; }

    public virtual CampainViewModel Campain { get; set; }
  }
}