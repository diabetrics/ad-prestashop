﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EurekapsBridge.Models
{
  public class LoginViewModel
  {
    [Required, DisplayName("Nombre de Usuario")]
    public string UserName { get; set; }
    [Required, DisplayName("Contraseña"), DataType(DataType.Password)]
    public string Password { get; set; }
  }
  public class CreateAccountViewModel
  {
    [Required(ErrorMessage = "La cedula es Requerida"), Display(Name = "Cedula")]
    public string ACP_User { get; set; }
    [Required(ErrorMessage = "El Correo Electronico es Requerido"), Display(Name = "Correo electronico"),
      DataType(DataType.EmailAddress, ErrorMessage = "Por favor ingrese una direccion de correo electronico valida")]
    public string ACP_Mail { get; set; }
    [Required(ErrorMessage = "El Nombre es requerido"), Display(Name = "Nombres")]
    public string ACP_FirstName { get; set; }
    [Required(ErrorMessage = "El Apellido es Requerido"), Display(Name = "Apellidos")]
    public string ACP_LastName { get; set; }
    [Required(ErrorMessage = "La contraseña es Requerida"), Display(Name = "Contraseña"), DataType(DataType.Password)]
    public string ACP_Password { get; set; }
    [Required(ErrorMessage = "La Confirmacion es requerida"), Display(Name = "Contraseña"), DataType(DataType.Password), Compare("ACP_Password", ErrorMessage = "Las Contraseñas no concuerdan")]
    public string ACP_ConfirmPassword { get; set; }

  }
  public class RestoreAccountViewModel
  {
    [Required(ErrorMessage = "La cedula es Requerida"), Display(Name = "Cedula")]
    public string ACP_User { get; set; }
    [Required(ErrorMessage = "El Correo Electronico es Requerido"), Display(Name = "Correo electronico"),
      DataType(DataType.EmailAddress, ErrorMessage = "Por favor ingrese una direccion de correo electronico valida")]
    public string ACP_Mail { get; set; }
  }

  public partial class eur_account
  {
    public long ShopId { get; set; }
  }

}