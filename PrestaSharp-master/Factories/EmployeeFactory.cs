﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Bukimedia.PrestaSharp.Factories
{
    public class EmployeeFactory : RestSharpFactory, IDisposable
    {
		public EmployeeFactory(string BaseUrl, string Account, string SecretKey)
            : base(BaseUrl, Account, SecretKey)
        {
        }

        public Entities.Employee Get(long EmployeeID)
        {
            RestRequest request = this.RequestForGet("employees", EmployeeID, "employee");
            return this.Execute<Entities.Employee>(request);
        }

      
         
        /// <summary>
        /// More information about filtering: http://doc.prestashop.com/display/PS14/Chapter+8+-+Advanced+Use
        /// </summary>
        /// <param name="Filter">Example: key:name value:Apple</param>
        /// <param name="Sort">Field_ASC or Field_DESC. Example: name_ASC or name_DESC</param>
        /// <param name="Limit">Example: 5 limit to 5. 9,5 Only include the first 5 elements starting from the 10th element.</param>
        /// <returns></returns>
        public List<Entities.Employee> GetByFilter(Dictionary<string, string> Filter, string Sort, string Limit)
        {
            RestRequest request = this.RequestForFilter("employees", "full", Filter, Sort, Limit, "employees");
            return this.ExecuteForFilter<List<Entities.Employee>>(request);
        }

         /// <summary>
        /// Get all customers.
        /// </summary>
        /// <returns>A list of customers</returns>
        public List<Entities.Employee> GetAll()
        {
            return this.GetByFilter(null, null, null);
        }
        
    
        public void Dispose()
        {
            this.Dispose();
        }
    }
}
