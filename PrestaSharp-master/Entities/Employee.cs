﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Bukimedia.PrestaSharp.Entities
{
    [XmlType(Namespace = "Bukimedia/PrestaSharp/Entities")]
    public class Employee : PrestaShopEntity
    {
        public long? id_employee { get; set; }
        public string id_profile { get; set; }
        public string id_lang { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string email { get; set; }
        public string passwd { get; set; }
        public string last_passwd_gen { get; set; }
        public string stats_date_from { get; set; }
        public string stats_date_to { get; set; }
        public string stats_compare_from { get; set; }
        public string stats_compare_to { get; set; }
        public string stats_compare_option { get; set; }
        public string preselect_date_range { get; set; }
        public string bo_color { get; set; }
        public string bo_theme { get; set; }
        public string bo_css { get; set; }
        public string default_tab { get; set; }
        public string bo_width { get; set; }
        public string bo_menu { get; set; }
        public string active { get; set; }
        public string optin { get; set; }
        public string id_last_order { get; set; }
        public string id_last_customer_messa { get; set; }
        public string id_last_customer { get; set; }
        public string last_connection_date { get; set; }
    }
}
